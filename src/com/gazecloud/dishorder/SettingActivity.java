package com.gazecloud.dishorder;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;

public class SettingActivity extends Activity {

	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.setting);
		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction fragmentTransaction	= fragmentManager.beginTransaction();
		SettingPreferenceFragment settingpf	= new SettingPreferenceFragment();
		fragmentTransaction.replace(android.R.id.content, settingpf);
		fragmentTransaction.commit();
		
	}

}
