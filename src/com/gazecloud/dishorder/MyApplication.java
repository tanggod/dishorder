package com.gazecloud.dishorder;

import android.app.Application;

/**
 * Android程序中访问资源时需要提供Context，
 * 一般来说只有在各种component中（Activity, Provider等等)才能方便的使用api来获取Context, 而在某些工具类中要获取就很麻烦了。
 * 为此，我们可以自定义一个Application类来实现这种功能。
 * 然后在manifest中<application>中加入name="mypackage.MyApplication"就可以在任意类中使用MyApplication.getInstance()来获取应用程序Context了。
 * @author tanggod
 */
public class MyApplication extends Application {
	private static MyApplication instance;

	public static MyApplication getInstance(){
		return instance;
	}
	/* (non-Javadoc)
	 * @see android.app.Application#onCreate()
	 */
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		instance = this;
	}
	
	

}
