package com.gazecloud.dishorder;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.seecloud.dishorder.BuildConfig;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.util.Log;

public class DishServer {

	//String urlDishItem1 = "http://www.duitang.com/album/1733789/masn/p/2/24";

	final String urlCommonDish = "/app/LocalUser/emenu/emenu/api.php?m=catalog&a=getcataloglist";
	final String urlComboDish = "/app/LocalUser/emenu/emenu/api.php?m=catalog&a=getcomboList";
	final String urlLogin = "/app/LocalUser/emenu/emenu/api.php?";
	final String urlVerify = "/app/LocalUser/emenu/emenu/api.php?";
	final String urlGetOrderNumber = "/app/LocalUser/emenu/emenu/api.php?m=user&a=newOrder";
	final String urlPutOrder = "/app/LocalUser/emenu/emenu/api.php?m=user&a=uploadOrderDetail";

	final String urlHome = "/app/LocalUser/emenu/emenu/";
	static final String defaultServer = "www.haoapp123.com";
	final String PASSWORD_KEY = "tanggod";
	private String TAG = "DishServer";

	Context mContext;
	private int defalutColumnCount = 4;
	private SharedPreferences prefs;

	DishServer() {
		mContext = MyApplication.getInstance().getApplicationContext();
		prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
	}

	public String getHomeAddress() {
		if (mContext == null || prefs == null) {
			return "";
		}
		String s = prefs.getString("server_key", defaultServer);
		if (s.length() == 0) {
			s = defaultServer;
		}
		s = "http://" + s;
		return s;
	}

	public String getCommonDishAddress() {
		//return urlCommonDish;
		if (mContext == null) {
			return "";
		}
		String s = getHomeAddress();

		s = s + urlCommonDish;
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "*** 普通菜品地址 ***");
			Log.d(TAG, s);
		}
		return s;
	}

	public String getComboDishAddress() {
		// TODO Auto-generated method stub
		//return urlCommonDish;
		if (mContext == null) {
			return "";
		}
		String s = getHomeAddress();

		s = s + urlComboDish;

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "--- 套餐菜品地址 ---");
			Log.d(TAG, s);
		}

		return s;
	}

	public String getImgAddress(String url) {
		// TODO Auto-generated method stub
		String s = getHomeAddress();

		s = s + urlHome + url;
		return s;
	}

	public int getColumnCount() {
		// TODO Auto-generated method stub
		if (mContext == null) {
			return defalutColumnCount;
		}
		return Integer.parseInt(prefs.getString("column_list_key", "4"));
	}

	public String getPrinterAddress(int n) {
		String ip = "";
		String homeAddress = "";

		if (prefs == null) {
			ip = homeAddress;
			return ip;
		}

		switch (n) {
		case 1:
			ip = prefs.getString("printer1_key", "");
			break;
		case 2:
			ip = prefs.getString("printer2_key", "");
			break;
		case 3:
			ip = prefs.getString("printer3_key", "");
			break;
		}
		return ip;
	}

	public String getPassword() {
		// TODO Auto-generated method stub
		String password = "";
		if (prefs == null) {
			password = getHomeAddress();
			return password;
		}

		password = prefs.getString(PASSWORD_KEY, "");
		return password;
	}

	public boolean setPassword(String value) {
		// TODO Auto-generated method stub
		Editor prefsEditor = prefs.edit();
		prefsEditor.putString(PASSWORD_KEY, value);
		prefsEditor.commit();
		return true;
	}

	public String getLoginAddress() {
		// TODO Auto-generated method stub
		if (mContext == null) {
			return "";
		}
		String s = getHomeAddress();
		s = s + urlLogin;
		return s;

	}

	public String getVerifyAddress(String encryptedKey) {
		// TODO Auto-generated method stub
		String s = getHomeAddress() + urlVerify + "m=user&a=device&devnum=" + encryptedKey;
		return s;
	}

	public int getPrinterCount() {
		// TODO Auto-generated method stub
		int count = 0;
		for (int i = 1; i <= 3; i++) {
			if (isValidAddress(getPrinterAddress(i))) {
				count++;
			}
		}
		return count;
	}

	public boolean isValidAddress(String ip) {
		// TODO Auto-generated method stub

		//String express = "(25[0-5]|2[0-4]\\d|1\\d{2}|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d{2}|[1-9]?\\d)) {3}";
		String express = "\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}";
		Pattern p = Pattern.compile(express);

		Matcher m = p.matcher(ip);

		boolean match = m.matches();

		if (!match) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, String.format("invalid ip [%s]", ip));
			}
			return false;
		}
		return true;
	}

	public String getOrderNumberAddress() {
		// TODO Auto-generated method stub
		if (mContext == null) {
			return "";
		}
		String s = getHomeAddress();

		s = s + urlGetOrderNumber;
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "*** 获取订单号的地址 ***");
			Log.d(TAG, s);
		}
		return s;
	}

	public String getPutOrderAddress() {
		// TODO Auto-generated method stub
		if (mContext == null) {
			return "";
		}
		String s = getHomeAddress();

		s = s + urlPutOrder;
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "*** 上传订单的地址 ***");
			Log.d(TAG, s);
		}
		return s;
	}

}
