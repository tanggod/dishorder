package com.gazecloud.dishorder;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ScrollView;

public class MyScrollView extends ScrollView {


	private boolean canScroll;
	 
    private GestureDetector mGestureDetector;
    View.OnTouchListener mGestureListener;
 
	public MyScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mGestureDetector = new GestureDetector(context, new YScrollDetector());
//        canScroll = true;
    }
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
//        if(ev.getAction() == MotionEvent.ACTION_UP)
//            canScroll = true;
    	boolean a = super.onInterceptTouchEvent(ev);
    	boolean b = mGestureDetector.onTouchEvent(ev);
    	
    	//Log.d("MyScroll", String.format("a: %s,  b:%s", Boolean.toString(a), Boolean.toString(b)));
        return a&b;
    }
 
    class YScrollDetector extends SimpleOnGestureListener {
        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
//        	Log.d("MyScroll", String.format("onScroll,  %s", Boolean.toString(canScroll)));
//            if(canScroll)
//                if (Math.abs(distanceY) >= Math.abs(distanceX))
//                    canScroll = true;
//                else
//                    canScroll = false;
            return true;
        }
    }
}
