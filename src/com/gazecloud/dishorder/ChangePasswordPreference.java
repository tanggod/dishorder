package com.gazecloud.dishorder;

import com.seecloud.dishorder.R;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.preference.DialogPreference;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ChangePasswordPreference extends DialogPreference {
	Context context;
	EditText	editText1, editText2, editText3;
	EditTextWatcher editTextWatcher = new EditTextWatcher();
	
	public class EditTextWatcher implements TextWatcher {

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			// TODO Auto-generated method stub
			onEditTextChanged();
		}

	}

	
	public ChangePasswordPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		this.context = context;
	}
	
	@Override
	protected View onCreateDialogView() {
		// TODO Auto-generated method stub

		// Inflate layout
		LayoutInflater inflater = (LayoutInflater) getContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.change_password, null);

		editText1 = (EditText)view.findViewById(R.id.editTextPassword1);
		editText2 = (EditText)view.findViewById(R.id.editTextPassword2);
		editText3 = (EditText)view.findViewById(R.id.editTextPassword3);
		
		editText1.addTextChangedListener(editTextWatcher);
		editText2.addTextChangedListener(editTextWatcher);
		editText3.addTextChangedListener(editTextWatcher);
		
		return view;
	}
	
	
	/* (non-Javadoc)
	 * @see android.preference.DialogPreference#onDialogClosed(boolean)
	 */
	@Override
	protected void onDialogClosed(boolean positiveResult) {
		// TODO Auto-generated method stub
		super.onDialogClosed(positiveResult);
		
		if (!positiveResult) {
			return;
		}
		
		if (shouldPersist()) {
			String password = editText2.getText().toString();

			DishServer dishServer = new DishServer();
			dishServer.setPassword(password);
		}

		// Notify activity about changes (to update preference summary line)
		notifyChanged();
		Toast.makeText(context, "password modified", Toast.LENGTH_SHORT).show();
	}

	public void onEditTextChanged() {
		// TODO Auto-generated method stub
		String oldPassword, newPassword1, newPassword2;
		String passwordSaved;
		
		oldPassword = editText1.getText().toString();
		newPassword1 = editText2.getText().toString();
		newPassword2 = editText3.getText().toString();
		
		DishServer dishServer = new DishServer();
		passwordSaved  = dishServer.getPassword();
		
		AlertDialog alertDlg = (AlertDialog)this.getDialog();
		Button btnOK	= alertDlg.getButton(AlertDialog.BUTTON_POSITIVE);
		btnOK.setEnabled(false);
		
		boolean isVerified = oldPassword.equals(passwordSaved);

		if( isVerified == false ){
			editText1.setError(Html.fromHtml("<font color=#808183>verified fail</font>"));
		}
		
		if( isVerified && newPassword1.length() >= 4 && newPassword1.equals(newPassword2)){
			btnOK.setEnabled(true);
		}else{
			btnOK.setEnabled(false);
		}
			
	}

	/* (non-Javadoc)
	 * @see android.preference.DialogPreference#showDialog(android.os.Bundle)
	 */
	@Override
	protected void showDialog(Bundle state) {
		// TODO Auto-generated method stub
		super.showDialog(state);
		
		AlertDialog alertDlg = (AlertDialog)this.getDialog();
		Button btnOK	= alertDlg.getButton(AlertDialog.BUTTON_POSITIVE);
		btnOK.setEnabled(false);

	}
}
