package com.gazecloud.dishorder;

import org.json.JSONException;
import org.json.JSONObject;

import com.seecloud.dishorder.BuildConfig;

import android.content.Context;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;

public class MyUtility {
	static String TAG = "MyUtility";

	/**
	 * 判断网络连接是否可用
	 * @param context
	 * @return true:可用，false:不可用
	 */
	public static boolean isNetworkAvailable(Context context) {
		// TODO Auto-generated method stub
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (cm == null) {
			return false;
		} else {
			// 如果仅仅是用来判断网络连接
			// 则可以使用 cm.getActiveNetworkInfo().isAvailable();
			NetworkInfo[] info = cm.getAllNetworkInfo();
			if (info != null) {
				for (int i = 0; i < info.length; i++) {
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * 根据JSONObject 取得对象的值，后面都要改成这种形式
	 * @param newsInfoLeftObject,	JSON对象
	 * @param key					在JSON对象中的键值
	 * @param defaultValue			如果找不到key，则返回的此值
	 * @return	
	 * 	string
	 */
	static public String getJSONValue(JSONObject newsInfoLeftObject, String key, String defaultValue) {
		// TODO Auto-generated method stub
		String value = defaultValue;
		try {
			value = newsInfoLeftObject.isNull(key) ? defaultValue : newsInfoLeftObject.getString(key);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return value;
	}

	static public String getDeviceKey(String mac) {
		// TODO Auto-generated method stub
		String key = "";
		String macBase64 = "unknown";
		try {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "mac: " + mac);
			}
			for (int i = 0; i < mac.length(); i++) {
				/**
				 * 去除冒号
				 */
				if (mac.charAt(i) == ':') {
					key = key + "囧";
				} else {
					key = key + mac.charAt(i);
				}
			}

			macBase64 = new String(Base64.encode(key.getBytes(), 0));
			macBase64 = macBase64.substring(0, macBase64.length() - 1);

			macBase64 = macBase64.substring(macBase64.length() / 2, macBase64.length());
			if (BuildConfig.DEBUG) {
				Log.d(TAG, String.format("Key : [%s]", macBase64));
			}

			getEncryptKey(macBase64);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return macBase64;
	}

	static public String getEncryptKey(String key) {
		String code = String.format("D%sd顶", key);
		code = code.replace('D', 'T');
		code = new String(Base64.encode(code.getBytes(), 0));
		code = code.substring(0, code.length() - 5);
		if (BuildConfig.DEBUG) {
			Log.d(TAG, String.format("[%s] ==> [%s]", key, code));
		}
		return code;
	}

	/**
	 * 根据是否具备电话功能判断是否平板，
	 * 不太准确
	 * @param context
	 * @return
	 */
	static public boolean isTabletDevice_old(Context context) {
		TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		int type = telephony.getPhoneType();
		if (type == TelephonyManager.PHONE_TYPE_NONE) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	    * 判断是否是平板,来自 Google I/O App for Android 
	    * @param context
	    * @return
	    */
	  static  public boolean isTabletDevice(Context context) {
	        return (context.getResources().getConfiguration().screenLayout
	                & Configuration.SCREENLAYOUT_SIZE_MASK)
	                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
	    }

}
