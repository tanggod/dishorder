package com.gazecloud.dishorder;

public class Constants {

	/**
	 * 显示列数
	 */
	public final static int COLUMN_COUNT = 4;

	// 每次加载30张图片
	//public final static int PICTURE_COUNT_PER_LOAD = 30; 

	/**
	 * 允许加载的最多图片数
	 */
	public final static int PICTURE_TOTAL_COUNT = 10000;

	public final static int HANDLER_WHAT = 1;

	public final static int MESSAGE_DELAY = 200;
	/**
	 *  only for internal use
	 */
	public static final String DISH_CONTENT = "dish_content";
	
	/**
	 * 菜品之间的横向间隔
	 */
	public final static int HORIZONTAL_GAP = 20;
	
	/**
	 * 菜品之间的纵向间隔
	 */
	public final static int VERTICAL_GAP = 20;

}
