package com.gazecloud.dishorder;

import java.io.IOException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import com.seecloud.dishorder.BuildConfig;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class Helper {
	
	static private String TAG = "Helper";

	private static final int REQUEST_TIMEOUT = 10 * 1000;//设置请求超时10秒钟  
	private static final int SO_TIMEOUT = 10 * 1000; //设置等待数据超时时间10秒钟  

	// 检测网络连接
	public static boolean checkConnection(Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager) context
				.getSystemService(context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
		if (networkInfo != null) {
			return networkInfo.isAvailable();
		}
		return false;
	}

	public static boolean isWifi(Context mContext) {
		ConnectivityManager connectivityManager = (ConnectivityManager) mContext
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
		if (activeNetInfo != null && activeNetInfo.getTypeName().equals("WIFI")) {
			return true;
		}
		return false;
	}

	/**
	 * 从网上获取内容get方式
	 * 
	 * @param url
	 * @return
	 * @throws IOException
	 * @throws ClientProtocolException
	 */
	public static String getStringFromUrl(String url) throws ClientProtocolException, IOException {
		HttpEntity entity = null;
		if( BuildConfig.DEBUG){
			Log.d(TAG,"*** getStringFromUrl begin");
		}
		try {
			HttpGet get = new HttpGet(url);
			BasicHttpParams httpParams = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(httpParams, REQUEST_TIMEOUT);
			HttpConnectionParams.setSoTimeout(httpParams, SO_TIMEOUT);

			HttpClient client = new DefaultHttpClient(httpParams);
			HttpResponse response = client.execute(get);
			entity = response.getEntity();
		} catch (Exception e) {
			Log.e("getStringFromUrl", "Exception!!!:" + url);
			e.printStackTrace();
		}
		if( BuildConfig.DEBUG){
			Log.d(TAG,"*** getStringFromUrl end");
		}
		return EntityUtils.toString(entity, "UTF-8");
	}

	public static String putDataToUrl(String url, JSONObject data) {
		// TODO Auto-generated method stub
		HttpClient httpClient = new DefaultHttpClient();
		HttpEntity entity;
		String res = "";
		try {

			Log.d("httpPost", data.toString());

			HttpPost httpPost = new HttpPost(url);

			httpPost.setEntity(new StringEntity(data.toString()));
			HttpResponse response = httpClient.execute(httpPost);

			entity = response.getEntity();

			res = EntityUtils.toString(entity);

			/**
			 * modified by tanggod on 2014-Jan-10
			 * refer to: http://stackoverflow.com/questions/4727114/illegalstateexception-content-has-been-consumed
			StringBuffer sb = new StringBuffer();
			BufferedReader reader;
			String s = null;
			while (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				entity = response.getEntity();
				reader = new BufferedReader(new InputStreamReader(entity.getContent()));
				while ((s = reader.readLine()) != null) {
					sb.append(s);
				}
				res = sb.toString();
			}
			*/
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}

	public static String putDataToUrl2(String url2, List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		HttpClient httpClient = new DefaultHttpClient();
		HttpEntity entity;
		String res = "";
		try {

			Log.d("httpPost", nameValuePair.toString());

			HttpPost httpPost = new HttpPost(url2);

			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair, "utf-8"));
			//			httpPost.setEntity(new StringEntity(nameValuePair.toString()));

			HttpResponse response = httpClient.execute(httpPost);

			entity = response.getEntity();

			res = EntityUtils.toString(entity);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}

}
