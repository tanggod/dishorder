package com.gazecloud.dishorder;

import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;

import org.apache.http.MalformedChunkCodingException;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.gazecloud.dish.Dish;
import com.gazecloud.dish.DishSet;
import com.haarman.listviewanimations.ArrayAdapter;
import com.haarman.listviewanimations.itemmanipulation.OnDismissCallback;
import com.haarman.listviewanimations.itemmanipulation.SwipeDismissAdapter;
import com.seecloud.dishorder.BuildConfig;
import com.seecloud.dishorder.R;
import com.viewpagerindicator.TabPageIndicator;
import com.zj.wfsdk.WifiCommunication;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class DishOrderMainActivity extends FragmentActivity implements DishOrderFragment.OnDishItemClickedListener,
		DishOrderFragment.OnDishNameClickedListener, DishListViewAdapter.OnOrderBillChangedListener, OnClickListener,
		WifiPrinter.OnPrintSucceed {

	private boolean slide_to_delete = false;
	public class VerifyInfo {
		String result;
		String result_text;

		VerifyInfo() {
			result = "0";
			result_text = "NONE";
		}

	}

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	//SectionsPagerAdapter mSectionsPagerAdapter;
	DishOrderPagerAdapter mDishOrderPagerAdapter;
	/**
	 * dish information get from WelcomeActivity
	 */
	DishSet mDishSet;
	//	List<Dish> mListDishInfo1;
	//	List<Dish> mListDishInfo2;
	//	List<Dish> mListDishInfo3;
	static View lastDishView = null;

	private long exitTime = 0;

	private ArrayList<Fragment> fragmentList;
	static final String TAG = "DishOrderMainActivity";

	SwipeDismissAdapter swipeDismissAdapter;
	DishListViewAdapter dishAdapter;
	String orderId;

	/*
	 * 非空ip地址个数
	 */
	int ipCount;

	/**
	 * 已点菜的总价格， 在onOrderBillChangedListener更新
	 */
	float mOrderPrice;

	/**
	 * 几个打印机启动了
	 */
	int connFlag = 0;
	/**
	 * 有效打印机的个数
	 */
	int printerCount = 0;
	/**
	 * 打印成功的次数
	 */
	int printSucceed = 0;

	WifiPrinter wifiPrinter;
	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;
	ListView mListView;
	TextView mTextViewBill;
	LinearLayout layoutDesc;
	TextView mTextViewDishDesc;
	ImageView imgSetting;
	TextView mTextViewSelected;
	/**
	 * 客户名
	 */
	TextView tvName;
	EditText edName;
	/**
	 * 桌号
	 */
	TextView tvTable;
	EditText edTable;

	Button btnClear;
	Button btnTake;

	/**
	 * 待打印的数据
	 */
	String dataTobePrinted;

	long lastTimestamp;
	int clickCount;
	WaitDialog waitDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dish_order_main);

		if (MyUtility.isTabletDevice(this)) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "This is a pad");
			}
			setContentView(R.layout.activity_dish_order_main);
		} else {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "This is a mobile phone");
			}
			setContentView(R.layout.activity_dish_order_main_ue);
		}

		orderId = "";
		mListView = (ListView) findViewById(R.id.listView1);
		mTextViewBill = (TextView) findViewById(R.id.tvBill);
		mTextViewDishDesc = (TextView) findViewById(R.id.tvDishDescription);
		layoutDesc = (LinearLayout) findViewById(R.id.layout_desc);
		btnClear = (Button) findViewById(R.id.btnClear);
		btnTake = (Button) findViewById(R.id.btnTake);
		imgSetting = (ImageView) findViewById(R.id.imgSetting);
		mTextViewSelected = (TextView) findViewById(R.id.tvSelected);

		View view1 = findViewById(R.id.name_layout);
		tvName = (TextView) view1.findViewById(R.id.login_input_text);
		edName = (EditText) view1.findViewById(R.id.edit_input);
		tvName.setText("Name");

		View view2 = findViewById(R.id.table_layout);
		tvTable = (TextView) view2.findViewById(R.id.login_input_text);
		edTable = (EditText) view2.findViewById(R.id.edit_input);
		tvTable.setText("Table No.");

		btnClear.setOnClickListener(this);
		btnTake.setOnClickListener(this);
		imgSetting.setOnClickListener(this);
		mTextViewSelected.setOnClickListener(this);

		wifiPrinter = new WifiPrinter(this);
		waitDialog = new WaitDialog(this, R.style.customDialog);

		dishAdapter = new DishListViewAdapter(DishOrderMainActivity.this);
		if( slide_to_delete ){
			swipeDismissAdapter = new SwipeDismissAdapter(dishAdapter, new ListViewDismissCallback(dishAdapter));
			swipeDismissAdapter.setListView(mListView);
			mListView.setAdapter(swipeDismissAdapter);
		}else{
			mListView.setAdapter(dishAdapter);
		}

		mDishSet = (DishSet) getIntent().getSerializableExtra(Constants.DISH_CONTENT);
		if (mDishSet == null) {
			Toast.makeText(this, "DishOrderMainActivity get data fail, please contact adminstrator.",
					Toast.LENGTH_SHORT).show();
		}

		initViewPager();
	}

	public class ListViewDismissCallback implements OnDismissCallback {

		private ArrayAdapter<DishListItem> mAdapter;

		public ListViewDismissCallback(DishListViewAdapter dishAdapter) {
			// TODO Auto-generated constructor stub
			mAdapter = dishAdapter;
		}

		@Override
		public void onDismiss(ListView listView, int[] reverseSortedPositions) {
			// TODO Auto-generated method stub
			for (int position : reverseSortedPositions) {
				//mAdapter.getItem(position).setDishSelected(false);
				try {
					if (position < mAdapter.getCount()) {
						((DishListViewAdapter) mAdapter).setDishUnselected(position);

						mAdapter.remove(position);
					} else {
						if (BuildConfig.DEBUG) {
							Log.e(TAG,
									String.format("ListView onDismiss() ，可能是删的太快了，下标越界[%d/%d]", position,
											mAdapter.getCount()));
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onStop()
	 */
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();

	}

	private void initViewPager() {
		// TODO Auto-generated method stub
		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		if (mDishSet == null) {
			return;
		}
		fragmentList = new ArrayList<Fragment>();

		List<String> title = new ArrayList<String>();
		/**
		 * 菜品
		 */
		for (Entry<String, List<Dish>> item : mDishSet.getSortedMapKey()) {
			String key = item.getKey();
			title.add(key);
			Fragment dishFragment = DishOrderFragment.newInstance(mDishSet.getDish().get(key));
			fragmentList.add(dishFragment);
		}
		//		for (String key : mDishSet.getDish().keySet()) {
		//			title.add(key);
		//			Fragment dishFragment = DishOrderFragment.newInstance(mDishSet.getDish().get(key));
		//			fragmentList.add(dishFragment);
		//		}

		mDishOrderPagerAdapter = new DishOrderPagerAdapter(getSupportFragmentManager(), this, fragmentList);

		mDishOrderPagerAdapter.setTabTitle(title);

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mDishOrderPagerAdapter);
		mViewPager.setOffscreenPageLimit(3);

		mViewPager.setOnPageChangeListener(new DishPagerChangeListener());

		//实例化TabPageIndicator然后设置ViewPager与之关联
		TabPageIndicator indicator = (TabPageIndicator) findViewById(R.id.tab_page_indicator);
		indicator.setViewPager(mViewPager);
		indicator.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub
			}

		});

	}

	public class DishPagerChangeListener implements OnPageChangeListener {

		@Override
		public void onPageScrollStateChanged(int arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onPageSelected(int arg0) {
			// TODO Auto-generated method stub
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "onPageSelected");
			}
		}

	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.dish_order_main, menu);
		return true;
	}

	/**
	 * 单击菜单事件，给ListView 添加数据
	 */
	@Override
	public void onDishItemClickedListener(DishListItem dishListItem, boolean isSelected) {
		// TODO Auto-generated method stub
		if (isSelected) {
			/**
			 * add data
			 */
			dishAdapter.add(dishListItem);
			
			/**
			 * listview 滚到最后面去
			 */
			mListView.setSelection(mListView.getAdapter().getCount() - 1);
		} else {
			/**
			 * remove data
			 */
			dishAdapter.remove(dishListItem);
		}
	}

	/**
	* 删除item，并播放动画
	* @param rowView 播放动画的view
	* @param positon 要删除的item位置
	*/
	protected void removeListItem(View rowView, final int positon) {
		try {
			/**
			 * 这段动画在Pad模拟器上有问题，后面再修改为Google的动画
			 */
			//			final Animation animation = (Animation) AnimationUtils
			//					.loadAnimation(rowView.getContext(), R.anim.item_anim);
			//			animation.setAnimationListener(new AnimationListener() {
			//				public void onAnimationStart(Animation animation) {
			//				}
			//
			//				public void onAnimationRepeat(Animation animation) {
			//				}
			//
			//				public void onAnimationEnd(Animation animation) {
			//					
			//					dishAdapter.removeItem(positon);
			//					animation.cancel();
			//					Log.d(TAG, "onAnimationEnd");
			//				}
			//			});
			//			rowView.startAnimation(animation);
			/**
			 * 后面再加google动画, Goolge_ListViewAnimations
			 */
			dishAdapter.remove(positon);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#dispatchTouchEvent(android.view.MotionEvent)
	 */
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		// TODO Auto-generated method stub
		return super.dispatchTouchEvent(ev);
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.action_settings:
			loginAndSetting();
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void verifyAndSetting() {
		DishServer dishServer = new DishServer();

		WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		WifiInfo info = wifi.getConnectionInfo();
		String mac = info.getMacAddress();

		String url = dishServer.getVerifyAddress(MyUtility.getDeviceKey(mac));

		DeviceVerifyTask deviceVerifyTask = new DeviceVerifyTask(this);

		if (BuildConfig.DEBUG) {
			Log.d(TAG, String.format("verify url: %s", url));
		}
		waitDialog.show();
		deviceVerifyTask.execute(url);
	}

	private void loginAndSetting() {
		// TODO Auto-generated method stub
		//		LoginLocalDialog loginDialog = new LoginLocalDialog(this, R.style.customDialog, R.layout.login_dialog);

		LoginRemoteDialog loginDialog = new LoginRemoteDialog(this, R.style.customDialog);
		loginDialog.show();
	}

	@Override
	public void onOrderBillChangedListener(int count, float price) {
		// TODO Auto-generated method stub
		mOrderPrice = price;
		mTextViewBill.setText(getString(R.string.bill, String.valueOf(count), price));
		mTextViewDishDesc.setText("");
		//		layoutDesc.setVisibility(View.GONE);
	}

	@Override
	public void onDishNameClickedListener(View v, Dish dishInfo) {
		// TODO Auto-generated method stub
		int visibility = layoutDesc.getVisibility();
		if (lastDishView == v && visibility == View.VISIBLE) {
			//			layoutDesc.setVisibility(View.GONE);
		} else {
			String desc = String.format("%s: %s", dishInfo.getName(), dishInfo.getDesc());
			mTextViewDishDesc.setText(desc);
			layoutDesc.setVisibility(View.VISIBLE);
		}

		/**
		 * commbo dish clicked
		 */
		if (dishInfo.getSubDish() != null) {
			Log.d(TAG, "show combo dish detail");
			SubDishDialogFragment dishFragment = SubDishDialogFragment.newInstance(dishInfo.getSubDish());
			dishFragment.show(getSupportFragmentManager(), "dialog");
		}
		lastDishView = v;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnClear:
			dishAdapter.clearData();
			break;
		case R.id.btnTake:
			TakeOrder();
			break;
		case R.id.imgSetting:

			loginAndSetting();
			break;
		case R.id.tvSelected:
			showKeyData();
			break;
		default:
			break;
		}
	}

	/**
	 * 读取MAC信息加密后显示，服务器配置次秘钥后才能登陆
	 */
	private void showKeyData() {
		// TODO Auto-generated method stub
		long currentTimestamp = System.currentTimeMillis();

		if (BuildConfig.DEBUG) {
			Log.d(TAG, String.format("time:%d, %d, %d", clickCount, currentTimestamp, currentTimestamp - lastTimestamp));
		}
		long gap = currentTimestamp - lastTimestamp;
		if (gap < 1000 && gap > 200) {
			clickCount++;
		} else {
			clickCount = 0;
		}
		if (clickCount > 10) {
			clickCount = 0;
			WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
			WifiInfo info = wifi.getConnectionInfo();
			String mac = info.getMacAddress();

			final String key = MyUtility.getDeviceKey(mac);
			Dialog dialog = new AlertDialog.Builder(this).setTitle("Key").setMessage(key)
					.setPositiveButton("copy to clipboard", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
							ClipData clip = ClipData.newPlainText("mac", key);
							clipboard.setPrimaryClip(clip);
						}
					}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
						}
					}).create();
			dialog.show();

		}
		lastTimestamp = currentTimestamp;
	}

	/**
	 * print order information
	 */
	private void TakeOrder() {
		// TODO Auto-generated method stub

		if (edName.getText().toString().trim().length() == 0 && edTable.getText().toString().trim().length() == 0) {
			Toast.makeText(this, "Take fail, please input Name or Table No.", Toast.LENGTH_SHORT).show();
			edName.startAnimation(AnimationUtils.loadAnimation(DishOrderMainActivity.this, R.anim.shake));
			edTable.startAnimation(AnimationUtils.loadAnimation(DishOrderMainActivity.this, R.anim.shake2));
			return;
		}
		if (dishAdapter.getCount() == 0) {
			Toast.makeText(this, "Please select dishes first", Toast.LENGTH_LONG).show();
			return;
		}

		dishAdapter.setOrderTime();
		AlertDialog.Builder builder = new Builder(DishOrderMainActivity.this);
		builder.setMessage("Are you sure to take the dish selected？");
		builder.setTitle("Confirm");
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub

				UploadOrderTask uploadOrderTask = new UploadOrderTask(DishOrderMainActivity.this);
				uploadOrderTask.execute(dishAdapter);

				dialog.dismiss();
			}

		});
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		builder.show();

	}

	private void printListProcess(){
		int LENGTH = 36;
		List<String> textList;
		
		textList = new ArrayList<String>();
		
		String line = "";

		textList.add("<N>");			//normal size
		line = "                 拿单(" + orderId + ")\n";		
		textList.add(line);
		
		line = String.format("Name:       %1$s\n", edName.getText());
		textList.add(line);

		line = String.format("Table No:   %1$s\n", edTable.getText());
		textList.add(line);

		line = String.format("Order Time: %s\n", dishAdapter.getOrderTime());
		textList.add(line);

		line = "================================================\n";
		textList.add(line);

		try {
			for (int i = 0; i < dishAdapter.getCount(); i++) {
				Dish dish = dishAdapter.getItem(i).getDish();
				textList.add("<N>");			//normal size
				textList.add(dish.getENName());
				textList.add("<B>");			//big size
				textList.add(dish.getCNName());
				textList.add("<N>");			//normal size
				textList.add(dish.getPrice());
				textList.add("  \n");
				//line = appendBlankChar(dish.getAllName(), LENGTH);
				//line = String.format("%1$s%2$12s\n", line, dish.getPrice());

				for (int j = 0; j < dishAdapter.getItem(i).getAccSelected().length; j++) {
					boolean accSelected = dishAdapter.getItem(i).getAccSelected()[j];
					if (accSelected) {
						String accName = dishAdapter.getItem(i).getDish().getAcc().get(j).getName();
						//getStrLength(accName);
						line = appendBlankChar("    " + accName, LENGTH);
						float accPrice = dishAdapter.getItem(i).getDish().getAcc().get(j).getPrice();
						line = String.format(Locale.getDefault(), "%1$s%2$12.2f\n", line, accPrice);
						textList.add(line);

					}
				}
			}
			textList.add("<N>");			//normal size
			line = "================================================\n\n";
			textList.add(line);

			line = String.format(Locale.getDefault(),"Total %1$d items price: %2$s", dishAdapter.getCount(), mOrderPrice);
			textList.add(line);

			PrinterTask printTask = new PrinterTask(this);
			wifiPrinter.setDataList(textList);
			
			printTask.execute(wifiPrinter);
			//printOrderBill(text);
			for( int i = 0; i < textList.size(); i++ ){
				Log.d(TAG, textList.get(i));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}
	private void printProcess() {
		int LENGTH = 36;
		String text = "";
		String line = "";

		text = "                 拿单(" + orderId + ")\n";
		line = String.format("Name:       %1$s\n", edName.getText());
		text = text + line;

		line = String.format("Table No:   %1$s\n", edTable.getText());
		text = text + line;

		line = String.format("Order Time: %s\n", dishAdapter.getOrderTime());
		text = text + line;

		line = "================================================\n";
		text = text + line;

		try {
			for (int i = 0; i < dishAdapter.getCount(); i++) {
				Dish dish = dishAdapter.getItem(i).getDish();

				line = appendBlankChar(dish.getAllName(), LENGTH);
				line = String.format("%1$s%2$12s\n", line, dish.getPrice());
				text = text + line;

				for (int j = 0; j < dishAdapter.getItem(i).getAccSelected().length; j++) {
					boolean accSelected = dishAdapter.getItem(i).getAccSelected()[j];
					if (accSelected) {
						String accName = dishAdapter.getItem(i).getDish().getAcc().get(j).getName();
						//getStrLength(accName);
						line = appendBlankChar("    " + accName, LENGTH);
						float accPrice = dishAdapter.getItem(i).getDish().getAcc().get(j).getPrice();
						line = String.format(Locale.getDefault(), "%1$s%2$12.2f\n", line, accPrice);
						text = text + line;
					}
				}
			}
			line = "================================================\n\n";
			text = text + line;

			line = String.format(Locale.getDefault(),"Total %1$d items price: %2$s", dishAdapter.getCount(), mOrderPrice);
			text = text + line;

			dataTobePrinted = text;

			PrinterTask printTask = new PrinterTask(this);

			wifiPrinter.setData(dataTobePrinted);
			printTask.execute(wifiPrinter);
			//printOrderBill(text);

			Log.d(TAG, text);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String appendBlankChar(String line, int n) {
		// TODO Auto-generated method stub
		String res = line;
		try {
			int len = line.getBytes("GBK").length;
			for (int i = 0; i < n - len; i++) {
				res = res + " ";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}

	/**
	 * 返回字符串长度，中文按照2个字符算
	 * @param str
	 */
	private int getStrLength(String str) {
		// TODO Auto-generated method stub
		if (str == null || str.length() < 0) {
			return 0;
		}
		int len = 0;
		char c;
		for (int i = str.length() - 1; i >= 0; i--) {
			c = str.charAt(i);
			if (c > 255) {
				/*
				 * GBK 编码格式 中文占两个字节
				 * UTF-8 编码格式中文占三个字节 len += 3;
				 */
				len += 2;
			} else {
				len++;
			}
		}
		return len;
	}

	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onKeyDown(int, android.view.KeyEvent)
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub

		if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
			if ((System.currentTimeMillis() - exitTime) > 2000) {
				Toast.makeText(getApplicationContext(), "Press again to exit!", Toast.LENGTH_SHORT).show();
				exitTime = System.currentTimeMillis();
			} else {
				finish();
				System.exit(0);
			}
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	protected void printMsg(WifiCommunication _printer) {
		// TODO Auto-generated method stub
		try {

			final WifiCommunication printer = _printer;
			printer.sendMsg(dataTobePrinted, "gbk");
			byte[] tail = new byte[3];
			tail[0] = 0x0A;
			tail[1] = 0x0D;
			printer.sndByte(tail);

			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					Toast.makeText(getApplicationContext(), "Print successful", Toast.LENGTH_SHORT).show();
					printer.close();
					connFlag--;
				}

			}, 1000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public class DeviceVerifyTask extends AsyncTask<String, Integer, VerifyInfo> {
		Context context;

		DeviceVerifyTask(Context context) {
			super();
			this.context = context;
		}

		@Override
		protected VerifyInfo doInBackground(String... params) {
			// TODO Auto-generated method stub

			String url = params[0];
			String json = "";
			VerifyInfo verifyInfo = new VerifyInfo();
			try {
				if (Helper.checkConnection(context)) {
					json = Helper.getStringFromUrl(url);
					verifyInfo = parseVerifyJSON(json);

				}
			} catch (UnknownHostException e) {
				verifyInfo.result = "E";
				verifyInfo.result_text = e.getMessage();
				Log.e(TAG, "UnknownHostException");
			} catch (MalformedChunkCodingException e) {
				verifyInfo.result = "E";
				verifyInfo.result_text = e.getMessage();
				Log.e(TAG, "MalformedChunkCodingException");
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (BuildConfig.DEBUG) {
				//Log.d(TAG, "get json:" + json);
			}
			return verifyInfo;
		}

		private VerifyInfo parseVerifyJSON(String json) {
			// TODO Auto-generated method stub
			VerifyInfo verifyInfo = new VerifyInfo();
			try {
				if (json != null) {
					JSONObject newsObject = new JSONObject(json);
					verifyInfo.result = MyUtility.getJSONValue(newsObject, "result", "0");
					verifyInfo.result_text = MyUtility.getJSONValue(newsObject, "result_text", " ");
				}
			} catch (JSONException e) {
				/**
				 * 如果写入的是一个错误的地址，那么返回不了json，这时候仍然要能让进入配置界面
				 */
				verifyInfo.result = "1";
				verifyInfo.result_text = e.getMessage();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return verifyInfo;
		}

		/* (non-Javadoc)
		 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
		 */
		@Override
		protected void onPostExecute(VerifyInfo verify) {
			// TODO Auto-generated method stub
			super.onPostExecute(verify);

			waitDialog.dismiss();
			if (verify.result.equals("1")) {
				LoginRemoteDialog loginDialog = new LoginRemoteDialog(context, R.style.customDialog);
				loginDialog.show();
			} else if (verify.result.equals("E")) {
				/**
				 * 连接服务器失败，直接进入配置界面
				 */
				Intent intent = new Intent(context, SettingActivity.class);
				context.startActivity(intent);
			} else {
			}

			Toast.makeText(context, verify.result_text, Toast.LENGTH_SHORT).show();

		}

	}

	public class PrinterTask extends AsyncTask<WifiPrinter, Integer, Boolean> {

		Context mContext;

		public PrinterTask(Context context) {
			// TODO Auto-generated constructor stub
			mContext = context;
		}

		/* (non-Javadoc)
		 * @see android.os.AsyncTask#onPreExecute()
		 */
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			printSucceed = 0;
		}

		@Override
		protected Boolean doInBackground(WifiPrinter... params) {
			// TODO Auto-generated method stub
			WifiPrinter printer = params[0];

			//String data = params[0];

			DishServer dishServer = new DishServer();
			String ip;

			int port = 9100;
			try {
				for (int i = 1; i <= 3; i++) {
					ip = dishServer.getPrinterAddress(i);
					if (ip.trim().length() > 0) {
						/**
						 * 计算非空的IP地址个数
						 */
						ipCount++;

						if (dishServer.isValidAddress(ip)) {
							printer.initAndPrint(ip, port);
						} else if (BuildConfig.DEBUG) {
							Log.d(TAG, String.format("Invalid printer ip:[%s]", ip));
						}
					}
					Thread.sleep(3000);
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return true;
		}

	}

	public class UploadOrderTask extends AsyncTask<DishListViewAdapter, Integer, ResultTakeOrder> {
		Context context;
		String orderIdGetFromServer = "";
		int errOrder = 0;

		/* (non-Javadoc)
		 * @see android.os.AsyncTask#onPreExecute()
		 */
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			waitDialog.show();
		}

		public UploadOrderTask(Context c) {
			context = c;
		}

		@Override
		protected ResultTakeOrder doInBackground(DishListViewAdapter... params) {

			// TODO Auto-generated method stub
			DishListViewAdapter listAdapter = params[0];

			DishServer dishServer = new DishServer();
			/**
			 * 请求订单单号的地址
			 */
			String url1;
			/**
			 * 上传菜单的地址
			 */
			String url2;

			ResultTakeOrder result = new ResultTakeOrder();

			url1 = dishServer.getOrderNumberAddress();
			url2 = dishServer.getPutOrderAddress();

			String json = "";

			errOrder = 0;

			try {
				if (Helper.checkConnection(context)) {

					/**
					 * 请求地址	http://www.haoapp123.com/app/LocalUser/emenu/emenu/api.php?m=user&a= newOrder
					请求方式	request
					输入参数	'name'
					'table_num'
					'totalprice'
					'ordertime'
					输出参数(json)	返回结果带 order_id ,用于上传详细记录
					 */

					url1 = String.format(Locale.getDefault(),
							"%1$s&name=%2$s&table_num=%3$s&totalprice=%4$s&ordertime=%5$s", url1, URLEncoder.encode(edName.getText().toString().trim()),
							URLEncoder.encode(edTable.getText().toString()), listAdapter.getAmount(), listAdapter.getOrderTime2());
					if (BuildConfig.DEBUG) {
						Log.d(TAG, "*** 获取订单号的地址及参数 ***");
						Log.d(TAG, url1);
					}
					json = Helper.getStringFromUrl(url1);
					if (BuildConfig.DEBUG) {
						Log.d(TAG, "*** 获取订单号时返回的JSON数据 ***");
						Log.d(TAG, json);
					}
					result = parseGetOrderIdJson(json);
					if (!result.code.equals("1")) {
						/**
						 * 返回值不等于1，表示获取订单号失败
						 */
						return result;
					}

					orderIdGetFromServer = result.id;

					/**
					 * 上传明细接口
					请求地址	http://www.haoapp123.com/app/LocalUser/emenu/emenu/api.php?m=user&a= uploadOrderDetail
					请求方式	request
					输入参数	dishname
					accnames
					dishprice
					acctotalprice
					order_id
					输出参数(json)	返回结果带 order_id ,用于上传详细记录
					 */
					for (int i = 0; i < listAdapter.getCount(); i++) {
						String url = String.format(
								"%1$s&dishname=%2$s&accnames=%3$s&dishprice=%4$s&acctotalprice=%5$s&order_id=%6$s",
								url2, URLEncoder.encode(listAdapter.getItem(i).getDish().getName()), URLEncoder.encode(listAdapter.getItem(i)
										.getAccNameSelected()), URLEncoder.encode(listAdapter.getItem(i).getDish().getPrice()), URLEncoder.encode(listAdapter
										.getItem(i).getAccTotAmountString()), orderIdGetFromServer);
						if (BuildConfig.DEBUG) {
							Log.d(TAG, "*** 上传菜单详细信息的地址及参数 ***");
							Log.d(TAG, url);
						}
						json = Helper.getStringFromUrl(url);
						if (BuildConfig.DEBUG) {
							Log.d(TAG, "*** 上传菜单详细信息到服务器时返回的数据 ***");
							Log.d(TAG, json);
						}

					}
					result = parsePutOrderJson(json);

				}
			} catch (Exception e) {
				errOrder++;
				result.code = "E";
				result.msg = "Exception: " + e.getMessage();
				e.printStackTrace();
			}
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "get json:" + json);
			}
			return result;
		}
		
		/**
		 * 这个方法可以一次性将数据上传到服务器，但服务器端不知道如何解析，所以暂时不用这种上传方法
		 * @param params
		 * @return
		 */
		protected ResultTakeOrder doInBackground_old(DishListViewAdapter... params) {
			// TODO Auto-generated method stub
			DishListViewAdapter listAdapter = params[0];

			DishServer dishServer = new DishServer();
			/**
			 * 请求订单单号的地址
			 */
			String url1;
			/**
			 * 上传菜单的地址
			 */
			String url2;

			ResultTakeOrder result = new ResultTakeOrder();

			url1 = dishServer.getOrderNumberAddress();
			url2 = dishServer.getPutOrderAddress();

			String json = "";

			try {
				if (Helper.checkConnection(context)) {
					json = Helper.getStringFromUrl(url1);
					if (BuildConfig.DEBUG) {
						Log.d(TAG, "*** 获取订单号时返回的JSON数据 ***");
						Log.d(TAG, json);
					}
					result = parseGetOrderIdJson(json);
					if (!result.code.equals("1")) {
						/**
						 * 返回值不等于1，表示获取订单号失败
						 */
						return result;
					}

					orderIdGetFromServer = result.id;

					JSONObject data = new JSONObject();
					JSONArray jsonArray = new JSONArray();

					data.put("name", edName.getText().toString());
					data.put("table", edTable.getText().toString());
					//data.put("order_id", orderIdGetFromServer);
					data.put("totalprice", listAdapter.getAmount());
					//data.put("dish_order", jsonArray);

					for (int i = 0; i < listAdapter.getCount(); i++) {
						JSONObject jsonObject = new JSONObject();

						jsonObject.put("dishname", listAdapter.getItem(i).getDish().getName());
						jsonObject.put("accnames", listAdapter.getItem(i).getAccNameSelected());
						jsonObject.put("acctotalprice", listAdapter.getItem(i).getAccTotAmountString());
						jsonObject.put("dishprice", listAdapter.getItem(i).getDish().getPrice());
						jsonArray.put(jsonObject);
					}

					List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
					nameValuePair.add(new BasicNameValuePair("DishOrder", data.toString()));
					nameValuePair.add(new BasicNameValuePair("OrderDetail", jsonArray.toString()));
					//json = Helper.putDataToUrl(url2, data);
					json = Helper.putDataToUrl2(url2, nameValuePair);
					if (BuildConfig.DEBUG) {
						Log.d(TAG, "*** 上传菜单到服务器时返回的数据 ***");
						Log.d(TAG, json);
					}

					result = parsePutOrderJson(json);

				}
			} catch (Exception e) {
				result.code = "E";
				result.msg = "Exception: " + e.getMessage();
				e.printStackTrace();
			}
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "get json:" + json);
			}
			return result;
		}

		private ResultTakeOrder parsePutOrderJson(String json) {
			// TODO Auto-generated method stub
			ResultTakeOrder r = new ResultTakeOrder();
			try {
				if (json != null) {
					JSONObject newsObject = new JSONObject(json);
					r.code = MyUtility.getJSONValue(newsObject, "result", "0");
					r.msg = MyUtility.getJSONValue(newsObject, "result_text", " ");
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				r.code = "E";
				r.msg = "Exception: " + e.getMessage();
				e.printStackTrace();
			}
			return r;
		}

		private ResultTakeOrder parseGetOrderIdJson(String json) {
			// TODO Auto-generated method stub
			ResultTakeOrder r = new ResultTakeOrder();
			try {
				if (json != null) {
					JSONObject newsObject = new JSONObject(json);
					r.code = MyUtility.getJSONValue(newsObject, "result", "0");
					r.msg = MyUtility.getJSONValue(newsObject, "result_text", " ");
					r.id = MyUtility.getJSONValue(newsObject, "order_id", " ");
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				r.code = "E";
				r.msg = "Exception: " + e.getMessage();
				e.printStackTrace();
			}
			return r;
		}

		/* (non-Javadoc)
		 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
		 */
		@Override
		protected void onPostExecute(ResultTakeOrder result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "*** 订单号 ***");
				Log.d(TAG, result.id);
				Log.d(TAG, result.msg);
			}
			orderId = orderIdGetFromServer;

			waitDialog.dismiss();

			if (BuildConfig.DEBUG) {
				Log.d(TAG, "*** 上传订单返回信息 *** ");
				Log.d(TAG, result.id);
				Log.d(TAG, result.msg);
			}

			if (result.code.equals("1")) {
				if (BuildConfig.DEBUG) {
					Log.d(TAG, "上传订单成功O(∩_∩)O~");
				}
				Toast.makeText(context, result.msg, Toast.LENGTH_LONG).show();
				//printProcess();
				printListProcess();
			} else if (result.code.equals("E")) {
				if (BuildConfig.DEBUG) {
					Log.d(TAG, "*** 上传订单失败 *** : " + errOrder);
				}
				String msg = String.format(Locale.getDefault(), "Upload dishes fail:[%d/%d],%s", errOrder,
						dishAdapter.getCount(), result.msg);
				Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(context, result.msg, Toast.LENGTH_LONG).show();
			}

			//printProcess();

		}

	}

	public class ResultTakeOrder {
		String code;
		String msg;

		/**
		 * order id
		 */
		String id;

		public ResultTakeOrder() {
			code = "";
			msg = "";
			id = "";
		}
	}

	@Override
	public void onPrintSucceedListener(String ip) {
		// TODO Auto-generated method stub
		printSucceed++;
		if (printSucceed >= printerCount) {
			/**
			 * 全部打印成功了，清空ListView
			 */
			dishAdapter.clear();
			clearUserInfo();
		}
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "print succeed:" + ip);
		}
	}
	public void clearUserInfo(){
		edName.setText("");
		edTable.setText("");
	}
}
