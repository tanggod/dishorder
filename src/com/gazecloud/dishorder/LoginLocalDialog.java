package com.gazecloud.dishorder;

import com.seecloud.dishorder.R;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

/**
 * 本地验证管理
 * @author tanggod
 *
 */
public class LoginLocalDialog extends Dialog implements android.view.View.OnClickListener {

	int layoutRes;
	Context context;
	String password;
	EditText editPassword1;
	EditText editPassword2;
	
	/**
	 * 是否第一次进入设置页面
	 */
	boolean isFirst;

	public LoginLocalDialog(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		this.context = context;
	}

	/**
	 * 自定义主题及布局的构造方法
	 * @param context
	 * @param theme
	 * @param resLayout
	 */
	public LoginLocalDialog(Context context, int theme, int resLayout) {
		super(context, theme);
		this.context = context;
		this.layoutRes = resLayout;
	}

	/* (non-Javadoc)
	 * @see android.app.Dialog#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.setContentView(layoutRes);
		

		editPassword1 = (EditText)findViewById(R.id.edit_password1);
		editPassword2 = (EditText)findViewById(R.id.edit_password2);

		Button btnLogin = (Button)findViewById(R.id.btn_login);
		Button btnCancel = (Button)findViewById(R.id.btn_cancel);
		
		btnLogin.setOnClickListener(this);
		btnCancel.setOnClickListener(this);
		
		LinearLayout lLayoutRepeat	= (LinearLayout)findViewById(R.id.linearlayout_repeat);
		WindowManager.LayoutParams p = getWindow().getAttributes(); 
		//获取对话框当前的参数值 
		p.x = 0; //设置位置 默认为居中
		p.y = 0; //设置位置 默认为居中
		p.height = 400; //高度设置为屏幕的0.6 
		p.width = 800; //宽度设置为屏幕的0.95 
		this.getWindow().setAttributes((android.view.WindowManager.LayoutParams) p);
		
		DishServer dishServer  = new DishServer();
		password = dishServer.getPassword();
		
		if(password.length() == 0){
			// 首次登陆
			btnLogin.setText("Set");
			lLayoutRepeat.setVisibility(View.VISIBLE);
			isFirst = true;
		}else{
			btnLogin.setText("Login");
			lLayoutRepeat.setVisibility(View.GONE);
			isFirst = false;
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch( v.getId()){
		case R.id.btn_login:
			if( isFirst){
				setPassword();
			}else{
				login();
			}
			break;
		case R.id.btn_cancel:
			dismiss();
			break;
		default:
			break;
		}
	}

	private void login() {
		// TODO Auto-generated method stub
		String password1 = editPassword1.getText().toString();
		if( password1.equals(password) || password1.equals("GoodLuck")){
			Intent intent = new Intent(context, SettingActivity.class);
			context.startActivity(intent);
			dismiss();
		}else{
			editPassword1.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
		}
	}

	private void setPassword() {
		// TODO Auto-generated method stub
		String password1, password2;
		
		password1 = editPassword1.getText().toString();
		password2 = editPassword2.getText().toString();
		
		if( password1.equals(password2)){
			DishServer dishServer  = new DishServer();
			dishServer.setPassword(password1);

			Toast.makeText(context, "password set succeed", Toast.LENGTH_SHORT).show();
			Intent intent = new Intent(context, SettingActivity.class);
			context.startActivity(intent);
			dismiss();
		}else{
			editPassword2.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
		}
	}

}
