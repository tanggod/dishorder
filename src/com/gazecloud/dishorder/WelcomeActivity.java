package com.gazecloud.dishorder;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.MalformedChunkCodingException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.gazecloud.dish.Dish;
import com.gazecloud.dish.DishAccessory;
import com.gazecloud.dish.DishSet;
import com.seecloud.dishorder.BuildConfig;
import com.seecloud.dishorder.R;
import com.tanggod.image.ImageLoader;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

public class WelcomeActivity extends Activity {

	private int fileDownloaded = 0;
	final String TAG = "WelcomeActivity";
	int totalDataCount;
	//List<DishInfo>	mListDishInfo;
	/**
	 * <菜品分类，菜品信息>
	 */
	//Map<String,List<Dish>> mDishMap;
	//List<Dish> mDishList;
	DishSet mDishSet;
	ProgressBar pb;
	Context mContext;
	boolean hasSDCard;

	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		/**
		 * the url, catalog stored
		 */
		/**
		 * screen height
		 */
		int screenHeight;
		/**
		 * screen width
		 */
		int screenWidth;

		super.onCreate(savedInstanceState);
		setContentView(R.layout.welcome);
		ImageView imgView = (ImageView) findViewById(R.id.imageView1);

		mContext = getApplicationContext();

		pb = (ProgressBar) findViewById(R.id.progressBar1);

		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		screenHeight = metrics.heightPixels;
		screenWidth = metrics.widthPixels;
		if (BuildConfig.DEBUG) {
			Log.d(TAG, String.format("screen: %d*%d", screenWidth, screenHeight));
		}
		Bitmap bitmap = ImageLoader.decodeSampledBitmapFromResource(getResources(), R.drawable.welcome, screenWidth,
				screenHeight);
		Drawable dr = new BitmapDrawable(bitmap);
		imgView.setBackgroundDrawable(dr);
		//		imgView.setImageBitmap(ImageLoader.decodeSamthepledBitmapFromResource(getResources(), R.drawable.welcome,
		//				screenWidth, screenHeight));

		if (MyUtility.isNetworkAvailable(WelcomeActivity.this) == false) {
			//Toast.makeText(WelcomeActivity.this, "Network not available, please check network configuration！", Toast.LENGTH_SHORT).show();
			AlertDialog.Builder builder = new Builder(WelcomeActivity.this);
			builder.setMessage("Network not available, DishOrder will exit.");
			builder.setTitle("Error");
			builder.setPositiveButton("OK", new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					dialog.dismiss();
					WelcomeActivity.this.finish();
				}

			});
			AlertDialog alertDialog = builder.create();
			alertDialog.setCancelable(false);
			alertDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
				@Override
				/**
				 * 拦截按键消息
				 * return true: 拦截
				 * 	      false: 不拦截
				 */
				public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
					// TODO Auto-generated method stub
					return true;
				}
			});
			alertDialog.show();
			return;
		}

		if (!hasSDCard()) {
			hasSDCard = false;
			if (BuildConfig.DEBUG) {
				Log.e(TAG, "No SD Card Found, Exits");
			}
			Toast.makeText(this, "No SD Card Found, DishOrder Exits", Toast.LENGTH_LONG).show();
			finish();
		} else {
			hasSDCard = true;
		}
		deviceVerify();
		/*
		 * 
		DishContentTask task = new DishContentTask(this);
		DishServer dishServer = new DishServer();
		urlCommonDish = dishServer.getCommonDish();
		urlComboDish = dishServer.getComboDish();
		if (!hasSDCard()) {
			hasSDCard = false;
			if (BuildConfig.DEBUG) {
				Log.e(TAG, "No SD Card Found, Exits");
			}
			Toast.makeText(this, "No SD Card Found, DishOrder Exits", Toast.LENGTH_LONG).show();
			finish();
		} else {
			hasSDCard = true;
			if (MyUtility.isNetworkAvailable(mContext) && task.getStatus() != Status.RUNNING) {
				task.execute(urlCommonDish, urlComboDish);
			}
		}
		*/
	}

	private void deviceVerify() {
		// TODO Auto-generated method stub
		DishServer dishServer = new DishServer();

		String mac = "unused";
		String url = dishServer.getVerifyAddress(MyUtility.getDeviceKey(mac));

		DeviceVerifyTask deviceVerifyTask = new DeviceVerifyTask(this);

		if (BuildConfig.DEBUG) {
			Log.d(TAG, String.format("verify url: %s", url));
		}
		deviceVerifyTask.execute(url);

	}

	public void dataDownloadComplete() {

		if (BuildConfig.DEBUG) {
		}
		if (mDishSet == null) {
			Toast.makeText(this, "get data fail, please contact adminstrator.", Toast.LENGTH_SHORT).show();
		} else {

			if (!hasSDCard) {
				return;
			}

			if (mDishSet.getSize() == 0) {
				imageFileDownloadComplete();
				return;
			}
			/**
			 *  下载图片
			 */
			pb.setMax(mDishSet.getSize());

			/**
			 * 遍历map的value
			 */
			String imgUrl;
			for (String key : mDishSet.getDish().keySet()) {
				List<Dish> list = mDishSet.getDish().get(key);
				for (int i = 0; i < list.size(); i++) {
					/**
					 * 根据value遍历list中的菜品列表
					 */
					imgUrl = list.get(i).getImgUrl();
					DownloadImageTask downloadTask = new DownloadImageTask();
					downloadTask.execute(imgUrl);
				}
			}
		}
	}

	private void imageFileDownloadComplete() {
		try {
			fileDownloaded++;

			pb.setProgress(fileDownloaded);
			if (fileDownloaded >= mDishSet.getSize()) {
				Log.i(TAG, String.format("download complete: %d", fileDownloaded));
				fileDownloaded = 0;

				Intent intent = new Intent(WelcomeActivity.this, DishOrderMainActivity.class);
				Bundle bundle = new Bundle(1);
				bundle.putSerializable(Constants.DISH_CONTENT, (Serializable) mDishSet);
				intent.putExtras(bundle);
				startActivity(intent);

				this.finish();

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 判断手机是否有SD卡。
	 * 
	 * @return 有SD卡返回true，没有返回false。
	 */
	private boolean hasSDCard() {
		return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
	}

	public class DishContentTask extends AsyncTask<String, Integer, DishSet> {

		private Context mContext;
		private DishSet dishSet;
		private String errMsg;

		public DishContentTask(Context context) {
			// TODO Auto-generated constructor stub
			super();
			mContext = context;
			dishSet = new DishSet();
		}

		/**
		 * para1: url for catalog
		 */
		@Override
		protected DishSet doInBackground(String... params) {
			// TODO Auto-generated method stub
			try {
				//parseAllDishJSON();
				return parseCatalogJSON(params[0], params[1]);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return dishSet;
		}

		/* (non-Javadoc)
		 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
		 */
		@Override
		protected void onPostExecute(DishSet result) {
			// TODO Auto-generated method stub
			//super.onPostExecute(result);

			showErrorMessage(errMsg);
			if ((result == null) || (result.getSize() <= 0)) {
				totalDataCount = 0;
			} else {
				totalDataCount = result.getSize();
			}
			mDishSet = result;

			dataDownloadComplete();
		}

		/* (non-Javadoc)
		 * @see android.os.AsyncTask#onPreExecute()
		 */
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

		private DishSet parseCatalogJSON(String urlCommonDish, String urlComboDish) {
			// TODO Auto-generated method stub
			String json = "";
			if (Helper.checkConnection(mContext)) {
				try {
					json = Helper.getStringFromUrl(urlCommonDish);
					parseCommonDishJSON(json);

				} catch (Exception e) {
					errMsg = String.format("get data ERROR:[%1$s] [%2$s]", e.toString(), urlCommonDish);
					e.printStackTrace();
				}
			}
			if (BuildConfig.DEBUG) {
				//Log.d(TAG, "get json:" + json);
			}

			if (Helper.checkConnection(mContext)) {
				try {
					json = Helper.getStringFromUrl(urlComboDish);
					parseComboDishJSON(json);

				} catch (Exception e) {
					errMsg = String.format("get data ERROR:[%1$s] [%2$s]", e.toString(), urlComboDish);
					e.printStackTrace();
				}
			}
			return dishSet;
		}

		private void parseComboDishJSON(String json) {
			// TODO Auto-generated method stub

			// TODO Auto-generated method stub
			try {
				if (dishSet == null) {
					Log.e(TAG, "dishSet = null");
					return;
				}

				DishServer dishServer = new DishServer();

				if (json != null) {
					JSONObject newsObject = new JSONObject(json);
					JSONArray listObject = newsObject.getJSONArray("list");
					for (int i = 0; i < listObject.length(); i++) {
						JSONObject newsInfoLeftObject = listObject.getJSONObject(i);
						String s;
						Dish comboDish = new Dish();
						//Dish dish = new Dish();

						/**
						 * 套餐名字
						 */
						//						s = newsInfoLeftObject.isNull("com_name") ? "null" : newsInfoLeftObject.getString("com_name");
						//						comboDish.setName(s);
						s = newsInfoLeftObject.isNull("com_name") ? "" : newsInfoLeftObject.getString("com_name");
						comboDish.setName(s);
						
						s = newsInfoLeftObject.isNull("com_name_cn") ? "" : newsInfoLeftObject.getString("com_name_cn");
						comboDish.setCNName(s);

						/**
						 * 套餐描述
						 */
						s = newsInfoLeftObject.isNull("com_desc") ? "" : newsInfoLeftObject.getString("com_desc");
						comboDish.setDesc(s);

						/**
						 * 套餐图片链接
						 */
						s = newsInfoLeftObject.isNull("com_photo") ? "" : newsInfoLeftObject.getString("com_photo");

						s = dishServer.getImgAddress(s);
						comboDish.setImgUrl(s);

						/**
						 * 菜品图片保存在本地的路径
						 */
						comboDish.setImgFile(getImagePath(s));

						s = newsInfoLeftObject.isNull("com_price") ? "0" : newsInfoLeftObject.getString("com_price");
						comboDish.setPrice(s);
						/**
						 * 套餐的分类，例如可以叫“套餐”, "Combo"等
						 */
						s = newsInfoLeftObject.isNull("com_catalog") ? "Combo" : newsInfoLeftObject
								.getString("com_catalog");
						comboDish.setCatalog(s);
						/**
						 * 获取辅料列表
						 */
						if (!newsInfoLeftObject.isNull("accessory")) {
							JSONArray accObject = newsInfoLeftObject.getJSONArray("accessory");
							for (int j = 0; j < accObject.length(); j++) {
								JSONObject accItemObject = accObject.getJSONObject(j);
								DishAccessory dishAccessory = new DishAccessory();

								s = accItemObject.isNull("acc_name") ? "" : accItemObject.getString("acc_name");
								dishAccessory.setName(s);

								comboDish.addAccItem(dishAccessory);
							}
						}

						/**
						 * 获取套餐内容（包含的菜品）
						 */
						JSONArray itemObject = newsInfoLeftObject.getJSONArray("catalog");
						for (int k = 0; k < itemObject.length(); k++) {
							JSONObject dishItemObject = itemObject.getJSONObject(k);
							Dish common = new Dish();
							s = dishItemObject.isNull("dish_name") ? "null" : dishItemObject.getString("dish_name");
							common.setName(s);

							s = dishItemObject.isNull("dish_disc") ? "" : dishItemObject.getString("dish_disc");
							common.setDesc(s);

							s = dishItemObject.isNull("dish_pic") ? "" : dishItemObject.getString("dish_pic");
							s = dishServer.getImgAddress(s);
							common.setImgUrl(s);

							/**
							 * 菜品图片保存在本地的路径
							 */
							common.setImgFile(getImagePath(s));

							comboDish.addSubDish(common);

						}
						dishSet.addDish(comboDish);
					}

				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		/**
		 * 根据JSONObject 取得对象的值，后面都要改成这种形式
		 * @param newsInfoLeftObject,	JSON对象
		 * @param key					在JSON对象中的键值
		 * @param defaultValue			如果找不到key，则返回的此值
		 * @return	
		 * 	string
		 */
		private String getJSONValue(JSONObject newsInfoLeftObject, String key, String defaultValue) {
			// TODO Auto-generated method stub
			String value = defaultValue;
			try {
				value = newsInfoLeftObject.isNull(key) ? defaultValue : newsInfoLeftObject.getString(key);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return value;
		}

		private void parseCommonDishJSON(String json) {
			// TODO Auto-generated method stub
			try {

				if (dishSet == null) {
					Log.e(TAG, "dishSet = null");
					return;
				}
				if (json != null) {
					JSONObject newsObject = new JSONObject(json);
					JSONArray listObject = newsObject.getJSONArray("list");
					for (int i = 0; i < listObject.length(); i++) {
						JSONObject newsInfoLeftObject = listObject.getJSONObject(i);
						String s;
						Dish dish = new Dish();
						//Dish dish = new Dish();

						/**
						 * 菜品名字
						 */
						s = newsInfoLeftObject.isNull("dish_name") ? "null" : newsInfoLeftObject.getString("dish_name");
						dish.setName(s);

						/**
						 * 菜品的中文名称，给厨房打印用
						 */
						s = newsInfoLeftObject.isNull("dish_name_cn") ? "null" : newsInfoLeftObject.getString("dish_name_cn");
						dish.setCNName(s);
						
						/**
						 * 菜品描述
						 */
						s = newsInfoLeftObject.isNull("dish_disc") ? "" : newsInfoLeftObject.getString("dish_disc");
						dish.setDesc(s);

						/**
						 * 图片链接
						 */
						s = newsInfoLeftObject.isNull("dish_pic") ? "" : newsInfoLeftObject.getString("dish_pic");
						DishServer dishServer = new DishServer();
						s = dishServer.getImgAddress(s);
						dish.setImgUrl(s);

						/**
						 * 菜品图片保存在本地的路径
						 */
						dish.setImgFile(getImagePath(s));

						s = newsInfoLeftObject.isNull("dish_price") ? "0" : newsInfoLeftObject.getString("dish_price");
						dish.setPrice(s);

						s = newsInfoLeftObject.isNull("type_name") ? "" : newsInfoLeftObject.getString("type_name");
						dish.setCatalog(s);
						/**
						 * 获取辅料列表
						 */
						if (!newsInfoLeftObject.isNull("acc")){
							
						JSONArray accObject = newsInfoLeftObject.getJSONArray("acc");
							for (int j = 0; j < accObject.length(); j++) {
								JSONArray accArrayObject = accObject.getJSONArray(j);
								JSONObject accItemObject = accArrayObject.getJSONObject(0);
								DishAccessory dishAccessory = new DishAccessory();
	
								s = accItemObject.isNull("acc_name") ? "null" : accItemObject.getString("acc_name");
								dishAccessory.setName(s);
	
								s = accItemObject.isNull("price") ? "0" : accItemObject.getString("price");
								dishAccessory.setPrice(s);
	
								dish.addAccItem(dishAccessory);
							}
						}
						dishSet.addDish(dish);
					}

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	/**
	 * 异步下载图片
	 * @author tanggod
	 *
	 */
	public class DownloadImageTask extends AsyncTask<String, Integer, String> {
		private String mImageUrl;

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			mImageUrl = params[0];

			/**
			 * download image file
			 */
			String imageFileName = loadImage(mImageUrl);
			return imageFileName;
		}

		/* (non-Javadoc)
		 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
		 */
		@Override
		protected void onPostExecute(String fileName) {
			// TODO Auto-generated method stub
			super.onPostExecute(fileName);

			if (BuildConfig.DEBUG) {
				Log.d(TAG, String.format("download complete: %s", fileName));
			}
			imageFileDownloadComplete();
		}

		/**
		 * 根据传入的URL，对图片进行加载。如果这张图片已经存在于SD卡中，则直接从SD卡里读取，否则就从网络上下载。
		 * 
		 * @param imageUrl
		 *            图片的URL地址
		 * @return 加载到内存的图片。
		 */
		private String loadImage(String url) {
			// TODO Auto-generated method stub
			/**
			 * download image file from url and save to sd card
			 * path + name
			 */
			String imageFileName = getImagePath(url);
			File imageFile = new File(imageFileName);
			//Log.i(TAG, String.format("file path: %s", path));
			if (!imageFile.exists()) {
				downloadImage(url, imageFileName);
			}
			return imageFileName;
		}

		/* (non-Javadoc)
		 * @see android.os.AsyncTask#onProgressUpdate(Progress[])
		 */
		@Override
		protected void onProgressUpdate(Integer... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
		}

		/**
		 * 将图片下载到SD卡缓存起来。
		 * 
		 * @param imageUrl
		 *            图片的URL地址。
		 */
		private void downloadImage(String imageUrl, String fileName) {
			if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
				//Log.d(TAG, "monted sdcard");
			} else {
				Log.e(TAG, "has no sdcard");
			}
			HttpURLConnection con = null;
			FileOutputStream fos = null;
			BufferedOutputStream bos = null;
			BufferedInputStream bis = null;
			File imageFile = null;
			try {
				URL url = new URL(imageUrl);
				con = (HttpURLConnection) url.openConnection();
				con.setConnectTimeout(5 * 1000);
				con.setReadTimeout(15 * 1000);
				con.setDoInput(true);
				//con.setDoOutput(true);
				//check status if Exception
				//int status = con.getResponseCode();
				bis = new BufferedInputStream(con.getInputStream());

				//imageFile = new File(getImagePath(imageUrl));
				imageFile = new File(fileName);
				fos = new FileOutputStream(imageFile);
				bos = new BufferedOutputStream(fos);
				byte[] b = new byte[1024];
				int length;
				while ((length = bis.read(b)) != -1) {
					bos.write(b, 0, length);
					bos.flush();
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (bis != null) {
						bis.close();
					}
					if (bos != null) {
						bos.close();
					}
					if (con != null) {
						con.disconnect();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	/**
	 * 获取图片的本地存储路径。
	 * 
	 * @param imageUrl
	 *            图片的URL地址。
	 * @return 图片的本地存储路径。
	 */
	public static String getImagePath(String imageUrl) {
		int lastSlashIndex = imageUrl.lastIndexOf("/");
		String imageName = imageUrl.substring(lastSlashIndex + 1);
		String imageDir = Environment.getExternalStorageDirectory().getPath() + "/DishOrder/";
		File file = new File(imageDir);
		if (!file.exists()) {
			file.mkdirs();
		}
		String imagePath = imageDir + imageName;
		return imagePath;
	}

	public void showToast(String text) {
		// TODO Auto-generated method stub
		Toast.makeText(this, text, Toast.LENGTH_LONG).show();

	}

	public void toastGetDataError() {
		// TODO Auto-generated method stub
		String s1, s2;
		DishServer dishServer = new DishServer();
		s1 = dishServer.getCommonDishAddress();
		s2 = dishServer.getComboDishAddress();
		if (BuildConfig.DEBUG) {
			Log.e(TAG, String.format("get data from server ERROR: [%1$s][%2$s]", s1, s2));
		}
		Toast.makeText(this, String.format("Get data fail,please check the configuration [%1$s][%2$s]", s1, s2),
				Toast.LENGTH_LONG).show();

	}

	public void showErrorMessage(String msg) {
		if (msg != null && msg.length() > 0) {
			Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
		}
	}

	public class DeviceVerifyTask extends AsyncTask<String, Integer, VerifyInfo> {
		Context context;

		DeviceVerifyTask(Context context) {
			super();
			this.context = context;
		}

		@Override
		protected VerifyInfo doInBackground(String... params) {
			// TODO Auto-generated method stub

			String url = params[0];
			String json = "";
			VerifyInfo verifyInfo = new VerifyInfo();
			try {
				if (Helper.checkConnection(context)) {
					json = Helper.getStringFromUrl(url);
					verifyInfo = parseVerifyJSON(json);

				}
			} catch (UnknownHostException e) {
				verifyInfo.result = "E";
				verifyInfo.result_text = e.getMessage();
				Log.e(TAG, "UnknownHostException");
			} catch (MalformedChunkCodingException e) {
				verifyInfo.result = "E";
				verifyInfo.result_text = e.getMessage();
				Log.e(TAG, "MalformedChunkCodingException");
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (BuildConfig.DEBUG) {
				//Log.d(TAG, "get json:" + json);
			}
			return verifyInfo;
		}

		private VerifyInfo parseVerifyJSON(String json) {
			// TODO Auto-generated method stub
			VerifyInfo verifyInfo = new VerifyInfo();
			try {
				if (json != null) {
					JSONObject newsObject = new JSONObject(json);
					verifyInfo.result = MyUtility.getJSONValue(newsObject, "result", "0");
					verifyInfo.result_text = MyUtility.getJSONValue(newsObject, "result_text", " ");
					if (!newsObject.isNull("list")) {
						JSONArray deviceArray = newsObject.getJSONArray("list");
						String deviceLicense;
						JSONObject deviceItem;
						for (int i = 0; i < deviceArray.length(); i++) {
							deviceItem = deviceArray.getJSONObject(i);
							deviceLicense = MyUtility.getJSONValue(deviceItem, "div_num", "NULL");
							verifyInfo.addLicense(deviceLicense);
						}
					}
				}
			} catch (JSONException e) {
				/**
				 * 如果写入的是一个错误的地址，那么返回不了json，这时候仍然要能让进入配置界面
				 */
				verifyInfo.result = "1";
				verifyInfo.result_text = e.getMessage();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return verifyInfo;
		}

		/* (non-Javadoc)
		 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
		 */
		@Override
		protected void onPostExecute(VerifyInfo verify) {
			// TODO Auto-generated method stub
			super.onPostExecute(verify);

			boolean isValidDevice = false;
			String toastMsg = "";

			toastMsg = verify.result_text;
			if (verify.result.equals("1")) {

				WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
				WifiInfo info = wifi.getConnectionInfo();
				String mac = info.getMacAddress();

				String key = MyUtility.getDeviceKey(mac);
				String deviceLicense = MyUtility.getEncryptKey(key);

				for (int i = 0; i < verify.deviceLicense.size(); i++) {
					if (verify.deviceLicense.get(i).equals(deviceLicense)) {
						isValidDevice = true;
						break;
					}
				}
				/**
				 * 调试用，如果服务器接口写好了，这句代码要删掉
				 */
				//isValidDevice = true;
				/**
				 * 调试用
				 */
				if (isValidDevice) {
					DishContentTask task = new DishContentTask(context);
					DishServer dishServer = new DishServer();
					String urlCommonDish, urlComboDish;

					urlCommonDish = dishServer.getCommonDishAddress();
					urlComboDish = dishServer.getComboDishAddress();
					if (MyUtility.isNetworkAvailable(mContext) && task.getStatus() != Status.RUNNING) {
						task.execute(urlCommonDish, urlComboDish);
					}
				} else {
					mDishSet = new DishSet();
					imageFileDownloadComplete();
					toastMsg = "invalid device";
				}

			} else if (verify.result.equals("E")) {
				/**
				 * 连接服务器失败，直接进
				 */
				mDishSet = new DishSet();
				imageFileDownloadComplete();
			} else {
				mDishSet = new DishSet();
				imageFileDownloadComplete();
			}
			Toast.makeText(context, toastMsg, Toast.LENGTH_LONG).show();
		}

		public boolean isValidKey(String remoteCode) {
			boolean ret = false;

			return ret;
		}

	}

	public class VerifyInfo {
		String result;
		String result_text;
		List<String> deviceLicense;

		VerifyInfo() {
			result = "0";
			result_text = "NONE";
			deviceLicense = new ArrayList<String>();
			//deviceLicense.add("VHVuTVRUbGTp");
		}

		public void addLicense(String license) {
			this.deviceLicense.add(license);
		}

	}

}
