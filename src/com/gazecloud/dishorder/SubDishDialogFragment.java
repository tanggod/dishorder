package com.gazecloud.dishorder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.gazecloud.dish.Dish;
import com.seecloud.dishorder.BuildConfig;
import com.seecloud.dishorder.R;
import com.tanggod.image.ImageLoader;
import com.tanggod.widget.FlowView;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SubDishDialogFragment extends DialogFragment {
	private ArrayList<LinearLayout> waterfall_items;
	private LinearLayout waterfall_container;

	static final String TITLE = "dish_catalog_name";
	ImageLoader imageLoader;

	/**
	 * 列高度
	 */
	private int[] column_height;
	private int[] lineIndex;

	/**
	 * log tag
	 */
	final String TAG = "DishOrderFragment";
	/**
	 * all dishes information, which got in WelcomeActivity
	 * WelcomeActivity -> DishOrderMainActivity -> DishOrderPagerAdapter -> DishOrderFragment
	 */
	private List<Dish> mListDishInfo;

	/**
	 * 菜单方块所占的宽度
	 */
	int item_width;

	int mWidth, mHeight;
	/**
	 * 显示列数
	 */
	int column_count = Constants.COLUMN_COUNT;

	Context mContext;

//	OnDishItemClickedListener dishItemClicked;
//	OnDishNameClickedListener dishNameClicked;

	public interface OnDishItemClickedListener {
		public void onDishItemClickedListener(DishListItem dishListItem, boolean isSelected);
	}

	public interface OnDishNameClickedListener {
		public void onDishNameClickedListener(View v, Dish dishInfo);
	}

	public static SubDishDialogFragment newInstance(List<Dish> list) {

		SubDishDialogFragment fragment = new SubDishDialogFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable(DishOrderFragment.TITLE, (Serializable) list);
		fragment.setArguments(bundle);
		return fragment;
	}

	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		DishServer dishServer = new DishServer();
		column_count = dishServer.getColumnCount();
		mListDishInfo = (List<Dish>) getArguments().getSerializable(DishOrderFragment.TITLE);
		column_height = new int[column_count];
		lineIndex = new int[column_count];
		imageLoader = ImageLoader.getInstance();

		setStyle(STYLE_NO_TITLE,0);
//		dishItemClicked = (OnDishItemClickedListener) getActivity();
//		dishNameClicked = (OnDishNameClickedListener) getActivity();

		if (BuildConfig.DEBUG) {
			Log.i(TAG, String.format("onCreate!!!, mListDishInfo size: %d", mListDishInfo.size()));
		}

	}

	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if (BuildConfig.DEBUG) {
			Log.d(TAG, String.format("onCreatView"));
		}
		super.onCreateView(inflater, container, savedInstanceState);

		mContext = getActivity();

		final View view = inflater.inflate(R.layout.subdish_layout, container, false);
		
		
		view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
			@Override
			public void onGlobalLayout() {
				// TODO Auto-generated method stub
				mHeight = view.getHeight();
				mWidth = view.getWidth();
				/**
				 * 获取每个LinearLayout的宽度
				 */
				item_width = (mWidth - column_count * Constants.HORIZONTAL_GAP * 2) / column_count;
				if (BuildConfig.DEBUG) {
					//Log.d(TAG, String.format("%d*%d, item_width=%d", mHeight, mWidth, item_width));
				}
			}
		});

		waterfall_container = (LinearLayout) view.findViewById(R.id.waterfall_container);
		waterfall_items = new ArrayList<LinearLayout>();
		for (int i = 0; i < column_count; i++) {
			LinearLayout itemLayout = new LinearLayout(mContext);

			// 此时 item_width 为0 ，需要等view创建后再设置LayoutParams才有效
			// LinearLayout.LayoutParams itemParam = new LinearLayout.LayoutParams(item_width, LayoutParams.WRAP_CONTENT);
			// itemLayout.setLayoutParams(itemParam);
			itemLayout.setPadding(2, 0, 2, 2);

			itemLayout.setOrientation(LinearLayout.VERTICAL);
			waterfall_items.add(itemLayout);
			waterfall_container.addView(itemLayout);
		}

		/**
		 * 需要延迟一点，否则view没有创建
		 * 就取不到mHeight, mWidth的值
		 * 导致item_width为0,添加的view看不见
		 */
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				for (Dish info : mListDishInfo) {
					LoadDishInfoTask task = new LoadDishInfoTask();
					task.execute(info);
				}
			}

		}, 500);

		return view;
	}

	protected void LoadDishInfoComplete(Bitmap bitmap, Dish _info) {
		final View convertView = LayoutInflater.from(mContext).inflate(R.layout.infos_list, null);
		final Dish _dishInfo = _info;

		/**
		 * 设置LinearLayout 的间距
		 */
		//		RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(item_width, LayoutParams.WRAP_CONTENT);
		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(item_width, LayoutParams.WRAP_CONTENT);
		/**
		 * 纵向间隔
		 */
		layoutParams.setMargins(0, Constants.VERTICAL_GAP, 0, Constants.VERTICAL_GAP);
		convertView.setLayoutParams(layoutParams);

		TextView priceView = (TextView) convertView.findViewById(R.id.dish_price);
		TextView titleView = (TextView) convertView.findViewById(R.id.dish_name);
		FlowView picv = (FlowView) convertView.findViewById(R.id.news_pic);

		int layoutHeight = item_width;
		//		LinearLayout.LayoutParams picParams = new LinearLayout.LayoutParams(item_width, layoutHeight);
		RelativeLayout.LayoutParams picParams = new RelativeLayout.LayoutParams(item_width, item_width);
		//		LinearLayout.LayoutParams picParams = new LinearLayout.LayoutParams(item_width, item_width);
		picv.set_url(_info.getImgUrl());
		picv.setLayoutParams(picParams);

		/**
		 * 长宽比例不变,图像不会变形
		 */
		//picv.setImageBitmap(bitmap);

		/**
		 * 长宽压缩到picv大小，图像可能会变形
		 */
		BitmapDrawable bd = new BitmapDrawable(bitmap);
		picv.setBackgroundDrawable((Drawable) bd);

		final String title = _info.getName();
		
		// 设置蓝色字体，套餐里面的菜品不能点击，所以取消
//		titleView.setText(Html.fromHtml("<u>" + _info.getName() + "</u>"));
		titleView.setText(_info.getName());
		/**
		 * 抗锯齿
		 */
		titleView.getPaint().setAntiAlias(true);

		titleView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				dishNameClicked.onDishNameClickedListener(v, _dishInfo);
			}

		});
		priceView.setText(_info.getPrice());
		priceView.setVisibility(View.GONE);
		int wspec = MeasureSpec.makeMeasureSpec(item_width, MeasureSpec.EXACTLY);
		convertView.measure(wspec, 0);

		int h = convertView.getMeasuredHeight();
		int w = convertView.getMeasuredWidth();
		//Log.d(TAG, "w:" + w + ",h:" + h);
		/**
		 * 计算列值
		 */
		int columnIndex = GetMinValue(column_height);
		picv.setColumnIndex(columnIndex);
		lineIndex[columnIndex]++;
		column_height[columnIndex] += h;

		if (item_width <= 0) {
			Log.e(TAG, String.format("ERROR: item_width: %d", item_width));
		}
		LinearLayout.LayoutParams itemParam = new LinearLayout.LayoutParams(item_width, LayoutParams.WRAP_CONTENT);

		/**
		 * 设置LinearLayout之间的横向间隔
		 */
		itemParam.setMargins(Constants.HORIZONTAL_GAP, 0, Constants.HORIZONTAL_GAP, 0);
		waterfall_items.get(columnIndex).setLayoutParams(itemParam);

		//convertView.setBackgroundDrawable(Utils.newSelector(mContext, -1, -1, -1, R.drawable.frame_selected, -1));
		waterfall_items.get(columnIndex).addView(convertView);

		final View dishView = convertView;
		picv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// TODO Auto-generated method stub
			}

		});
	}

	private int GetMinValue(int[] array) {
		int m = 0;
		int length = array.length;
		for (int i = 0; i < length; ++i) {

			if (array[i] < array[m]) {
				m = i;
			}
		}
		return m;
	}

	public class LoadDishInfoTask extends AsyncTask<Dish, Integer, Bitmap> {

		Dish dishInfo;
		String fileName;

		@Override
		protected Bitmap doInBackground(Dish... params) {
			// TODO Auto-generated method stub
			dishInfo = params[0];
			//fileName = WelcomeActivity.getImagePath(dishInfo.getIsrc());
			fileName = dishInfo.getImgFile();

			Bitmap imageBitmap = imageLoader.getBitmapFromMemoryCache(fileName);
			if (imageBitmap == null) {
				imageBitmap = loadImageFromSDCard(fileName);
			}
			return imageBitmap;
		}

		private Bitmap loadImageFromSDCard(String fileName) {
			// TODO Auto-generated method stub
			Bitmap bitmap = ImageLoader.decodeSampledBitmapFromResource(fileName, item_width);
			if (bitmap != null) {
				imageLoader.addBitmapToMemoryCache(fileName, bitmap);
				return bitmap;
			}
			return null;
		}

		/* (non-Javadoc)
		 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
		 */
		@Override
		protected void onPostExecute(Bitmap bitmap) {
			// TODO Auto-generated method stub
			super.onPostExecute(bitmap);
			LoadDishInfoComplete(bitmap, dishInfo);
		}

	}
}
