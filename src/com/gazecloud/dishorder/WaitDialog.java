package com.gazecloud.dishorder;

import com.seecloud.dishorder.R;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

public class WaitDialog extends Dialog {

	Context context;
	public WaitDialog(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		this.context = context;
	}
	public WaitDialog(Context context, int theme) {
		// TODO Auto-generated constructor stub
		super(context, theme);
		this.context = context;
	}
	/* (non-Javadoc)
	 * @see android.app.Dialog#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.wait);
		this.setCanceledOnTouchOutside(false);
	}

}
