package com.gazecloud.dishorder;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

public class DishOrderPagerAdapter extends FragmentStatePagerAdapter {
	String TAG = "DishOrderPagerAdapter";

	ArrayList<Fragment>	mFragmentList;
	List<String> tabTitle;

	/**
	 *  dish item count
	 */
	int totalDataCount = 0;
	Context mContext;

	public DishOrderPagerAdapter(FragmentManager fm, Context context, ArrayList<Fragment> fragmentList) {
		super(fm);
		mContext = context;
		mFragmentList	= fragmentList;
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentPagerAdapter#instantiateItem(android.view.ViewGroup, int)
	 */
	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		// TODO Auto-generated method stub
		
		return super.instantiateItem(container, position);
	}

	@Override
	public Fragment getItem(int position) {
		// TODO Auto-generated method stub
		//		String url;
		//		Fragment fragment = DishOrderFragment.newInstance("dishorder 菜品分类" + position);
		return mFragmentList.get(position);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mFragmentList.size();
	}

	/* (non-Javadoc)
	 * @see android.support.v4.view.PagerAdapter#getPageTitle(int)
	 */
	@Override
	public CharSequence getPageTitle(int position) {
		// TODO Auto-generated method stub
		return tabTitle.get(position);
	}

	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentPagerAdapter#destroyItem(android.view.ViewGroup, int, java.lang.Object)
	 */
	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		// TODO Auto-generated method stub
		super.destroyItem(container, position, object);
	}

	public void setTabTitle(List<String> title) {
		// TODO Auto-generated method stub
		tabTitle = title;
	}
}
