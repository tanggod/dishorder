package com.gazecloud.dishorder;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.gazecloud.dish.Dish;
import com.seecloud.dishorder.R;

public class DishListItem {
	String TAG = "DishListItem";
	/**
	 * 对应的菜品
	 */
	private Dish dishItem;
	/**
	 * 这个菜品点的份数
	 */
	private int dishCount;
	/**
	 * 待选菜品区的那个方块
	 */
	private View viewDishItem;
	/**
	 * 选择的辅料
	 */
	private boolean[] accSelected;

	public DishListItem(Dish _dishInfo, View dishView, boolean[] acc) {
		// TODO Auto-generated constructor stub
		dishItem = _dishInfo;
		viewDishItem = dishView;
		accSelected = acc;
		dishCount = 0;
	}

	public Dish getDish() {
		// TODO Auto-generated method stub
		return dishItem;
	}

	public void setDishSelected(boolean b) {
		// TODO Auto-generated method stub
		TextView textViewDishCount  = (TextView)viewDishItem.findViewById(R.id.textView_DishCount);
		String s = textViewDishCount.getText().toString();
		if( s.equals("")){
			s = "0";
		}
		int n = Integer.parseInt(s);
		
		if( b ){
			//true
			n++;
			s = String.valueOf(n);
			textViewDishCount.setText(s);
			textViewDishCount.setVisibility(View.VISIBLE);
			viewDishItem.setSelected(b);
		}else{
			// false
			n--;
			s = String.valueOf(n);
			textViewDishCount.setText(s);
			if( n < 1){
				n = 0;
				textViewDishCount.setVisibility(View.GONE);
				viewDishItem.setSelected(b);
			}
		}
	}

	public boolean[] getAccSelected() {
		// TODO Auto-generated method stub
		return accSelected;
	}

	public float getAccTotAmount() {
		// TODO Auto-generated method stub
		float tot = 0;
		if (dishItem.getAccSize() != accSelected.length) {
			Log.d(TAG, "getAccTotAmount() ERROR, dishItem.getAcc().size() != accSelected.length");
		}
		for (int i = 0; i < accSelected.length; i++) {
			tot = tot + (accSelected[i] ? dishItem.getAcc().get(i).getPrice() : 0);
		}
		return tot;
	}

	/**
	 * 将所有的辅料名称合并在一起，返回
	 * @return String
	 */
	public String getAccNameSelected() {
		// TODO Auto-generated method stub
		if (dishItem.getAccSize() != accSelected.length) {
			Log.d(TAG, "getAccNameSelected() ERROR, dishItem.getAcc().size() != accSelected.length");
		}

		String acc = "-";
		for( int i = 0; i < dishItem.getAccSize(); i++){
			acc = acc + (accSelected[i]?dishItem.getAcc().get(i).getName()+",":"");
		}
		return acc;
	}

	public String getAccTotAmountString() {
		// TODO Auto-generated method stub
		String price = String.format("%1$.2f", getAccTotAmount());
		return price;
	}

	public int getAccSize() {
		return dishItem.getAccSize();
	}
	public String[] getAccArray() {
		// TODO Auto-generated method stub
		String[] accName = new String[dishItem.getAccSize()];
		for( int i = 0; i < dishItem.getAccSize(); i++){
			accName[i] = dishItem.getAcc().get(i).getName();
		}
		return accName;
	}
	public String[] getAccPriceArray() {
		// TODO Auto-generated method stub
		String[] accName = new String[dishItem.getAccSize()];
		for( int i = 0; i < dishItem.getAccSize(); i++){
			accName[i] = String.format("%1$s, %2$.2f",dishItem.getAcc().get(i).getName(), dishItem.getAcc().get(i).getPrice()); 
		}
		return accName;
	}
	public void setAccSelected(boolean[] acc) {
		// TODO Auto-generated method stub
		if( acc.length > accSelected.length ){
			Log.d(TAG, "setAccSelected() ERROR:Length()");
		}
		for( int i = 0; i < acc.length; i++){
			accSelected[i] = acc[i];
		}
	}
}
