package com.gazecloud.dishorder;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.seecloud.dishorder.BuildConfig;
import com.zj.wfsdk.WifiCommunication;

public class WifiPrinter {

	String TAG = "WifiPrinter";
	Context context;
	WifiCommunication wfComm;
	String ip;
	int port;

	String dataTobePrinted;
	List<String> dataList;
	OnPrintSucceed onPrintSucceed;

	/**
	 * 打印成功接口，通知MainActivity打印完成一次
	 * @author tanggod
	 *
	 */
	public interface OnPrintSucceed {
		public void onPrintSucceedListener(String ip);
	}

	WifiPrinter(Context ct) {
		context = ct;
		wfComm = new WifiCommunication(handler);
		onPrintSucceed = (OnPrintSucceed)context;
		dataList = new ArrayList<String>();
	}

	public boolean initAndPrint(String _ip, int _port) {
		if (wfComm == null) {
			return false;
		} else {
			ip = _ip;
			port = _port;
			wfComm.initSocket(ip, port);
			return true;
		}

	}
	
	public void setData(String data){
		dataTobePrinted = data;
	}
	public void setDataList(List<String> list){
		dataList = list;
	}

	Handler handler = new Handler() {

		/* (non-Javadoc)
		 * @see android.os.Handler#handleMessage(android.os.Message)
		 */
		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			String text;
			super.handleMessage(msg);

			switch (msg.what) {
			case WifiCommunication.WFPRINTER_CONNECTED:
				print();
				onPrintSucceed.onPrintSucceedListener(ip);
				text = "Print successfully";
				if( BuildConfig.DEBUG){
					Log.d(TAG, String.format("[%s] to be printed", ip));
				}
				break;
			case WifiCommunication.WFPRINTER_DISCONNECTED:
				text = "Disconnect the WIFI-printer successful [" + ip + "]";
				break;
			case WifiCommunication.SEND_FAILED:
				text = "Send Data Failed,please reconnect [" + ip + "]";
				break;
			case WifiCommunication.WFPRINTER_CONNECTEDERR:
				text = "Connect the WIFI-printer error [" + ip + "]";
				break;
			case WifiCommunication.CONNECTION_LOST:
				text= "Connect the WIFI-printer error [" + ip + "]";
				break;
			default:
				text = "ERROR message received from printer [" + ip + "]";
				break;
			}
//			text = text + String.format(" [%s]", ip);
			Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
			if( BuildConfig.DEBUG){
				
				Log.d(TAG, text);
			}
			/**
			 * 测试用，正式发布需要删掉
			 */
			//onPrintSucceed.onPrintSucceedListener(ip);

			close();
		}

	};

	public boolean print() {
		// TODO Auto-generated method stub
		
    	byte[] cmdBigSize = new byte[3];
    	cmdBigSize[0] = 0x1b;
    	cmdBigSize[1] = 0x21;
    	cmdBigSize[2] |= 0x10; 
    	
    	byte[] cmdNormalSize = new byte[3];
    	cmdNormalSize[0] = 0x1b;
    	cmdNormalSize[1] = 0x21;
    	cmdNormalSize[2] &= 0xEF; 

    	byte[] cmdFont2 = {0x1d, 0x21, 0x11};
    	byte[] cmdFont4 = {0x1d, 0x21, 0x33};
    	byte[] cmdCutPaper = {0x1d, 0x56, 0x00};
		byte[] tail = new byte[3];
		tail[0] = 0x0A;
		tail[1] = 0x0D;
	    
		if (wfComm == null) {
			return false;
		}
		try {
			String line;
			String bigSize = "<B>";
			String normalSize = "<N>";
			
			for( int i = 0; i < dataList.size(); i++){
				line = dataList.get(i);
				if( line.compareTo(bigSize) == 0 ){
					wfComm.sndByte(cmdFont4);
				}else if( line.compareTo(normalSize) == 0){
					wfComm.sndByte(cmdFont2);
				}else{
					wfComm.sendMsg(line, "GBK");
				}
			}
			
			for( int i = 0; i < 8; i++){
				wfComm.sendMsg("\n", "GBK");
			}
			wfComm.sndByte(tail);
			wfComm.sndByte(cmdCutPaper);

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	public void close() {
		wfComm.close();
	}
	public boolean closePrinter() {
		if (wfComm != null) {
			wfComm.close();
			wfComm = null;
		}
		return true;
	}
}
