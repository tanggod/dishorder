package com.gazecloud.dishorder;

import com.seecloud.dishorder.R;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

public class SettingPreferenceFragment extends PreferenceFragment implements OnSharedPreferenceChangeListener {
	private final String TAG = "SettingPreferenceFragment";
	private final String SERVER = "server_key";
	private final String PRINTER1 = "printer1_key";
	private final String PRINTER2 = "printer2_key";
	private final String PRINTER3 = "printer3_key";
	private final String COLUMN = "column_list_key";

	EditTextPreference edtpServer;
	EditTextPreference edtpPrinter1;
	EditTextPreference edtpPrinter2;
	EditTextPreference edtpPrinter3;
	ListPreference listPreference;
	SharedPreferences sharedPreferences;

	/**
	 * Avoid non-default constructors in fragments: 
	 * use a default constructor plus Fragment#setArguments(Bundle) instead
	 */
	public SettingPreferenceFragment() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see android.preference.PreferenceFragment#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
		edtpServer = (EditTextPreference) findPreference(SERVER);
		edtpPrinter1 = (EditTextPreference) findPreference(PRINTER1);
		edtpPrinter2 = (EditTextPreference) findPreference(PRINTER2);
		edtpPrinter3 = (EditTextPreference) findPreference(PRINTER3);
		listPreference = (ListPreference) findPreference(COLUMN);

	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		// TODO Auto-generated method stub
		String text;
		String summary;

		if (key.equals(SERVER)) {
			text = edtpServer.getText();
			summary = getString(R.string.server_summary, text);
			edtpServer.setSummary(summary);
		} else if (key.equals(PRINTER1)) {
			text = edtpPrinter1.getText();
			summary = getString(R.string.printer_summary, text);
			edtpPrinter1.setSummary(summary);
		} else if (key.equals(PRINTER2)) {
			text = edtpPrinter2.getText();
			summary = getString(R.string.printer_summary, text);
			edtpPrinter2.setSummary(summary);
		} else if (key.equals(PRINTER3)) {
			text = edtpPrinter3.getText();
			summary = getString(R.string.printer_summary, text);
			edtpPrinter3.setSummary(summary);
		} else if (key.equals(COLUMN)) {
			text = listPreference.getEntry().toString();
			summary = getString(R.string.column_layout_summary, text);
			listPreference.setSummary(summary);
		}
	}

	/* (non-Javadoc)
	 * @see android.app.Fragment#onResume()
	 */
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		String s;

		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
		sharedPreferences.registerOnSharedPreferenceChangeListener(this);

		s = edtpServer.getText();
		if(s == null || s.equals("")){
			s = DishServer.defaultServer;
		}
		edtpServer.setSummary(getString(R.string.server_summary, s));

		s = edtpPrinter1.getText();
		edtpPrinter1.setSummary(getString(R.string.printer_summary, s));

		s = edtpPrinter2.getText();
		edtpPrinter2.setSummary(getString(R.string.printer_summary, s));

		s = edtpPrinter3.getText();
		edtpPrinter3.setSummary(getString(R.string.printer_summary, s));

		if (listPreference.getEntry() != null) {
			s = listPreference.getEntry().toString();
		}else{
			s = "4";
		}
		listPreference.setSummary(getString(R.string.column_layout_summary, s));

	}

	/* (non-Javadoc)
	 * @see android.app.Fragment#onPause()
	 */
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		sharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
	}

}
