package com.gazecloud.dishorder;

import java.text.SimpleDateFormat;

import com.gazecloud.dish.Dish;
import com.haarman.listviewanimations.ArrayAdapter;
import com.seecloud.dishorder.R;
import com.tanggod.image.ImageLoader;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class DishListViewAdapter extends ArrayAdapter<DishListItem> {
	private final String TAG = "DishListViewAdapter";
	Context mContext;
	LayoutInflater inflater;
	/**
	 * 下单时间
	 */
	String orderTime;

	/*
	 * 总账单
	 */
	int mBill;

	OnOrderBillChangedListener billChanged;

	public interface OnOrderBillChangedListener {
		void onOrderBillChangedListener(int count, float price);
	}

	public DishListViewAdapter(Context context) {
		// TODO Auto-generated constructor stub

		mContext = context;
		inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		billChanged = (OnOrderBillChangedListener) mContext;
		mBill = 0;
		orderTime = "";
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder = null;
		try {
			if (convertView == null) {
				if (MyUtility.isTabletDevice(mContext)) {
					convertView = inflater.inflate(R.layout.listview_item, null);
				} else {
					convertView = inflater.inflate(R.layout.listview_item_ue, null);
				}
				viewHolder = new ViewHolder(convertView);
				convertView.setTag(viewHolder);

			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}

			final DishListItem dishListItem = getItem(position);
			final Dish dishInfo = getItem(position).getDish();
			viewHolder.tvDishName.setText(dishInfo.getName());

			viewHolder.tvDishPrice.setText(dishInfo.getPrice());
			float price = Float.parseFloat(dishInfo.getPrice());
			mBill += price;

			final ViewHolder _viewHolder = viewHolder;
			final int _position = position;
			/**
			 * the JVM doesn't know how to blindly downcast Object[] (the result of toArray()) to String[]. 
			 * To let it know what your desired object type is, you can pass a typed array into toArray(). 
			 * The typed array can be of any size (new String[1] is valid), 
			 * but if it is too small then the JVM will resize it on it's own. 
			 */
			final String[] accName = getItem(position).getAccPriceArray();
			final boolean[] accSelected = getItem(position).getAccSelected();

			viewHolder.tvAcc.setText(getItem(position).getAccNameSelected());
			viewHolder.tvAccPrice.setText(getItem(position).getAccTotAmountString());
			
			if( dishListItem.getAccSize() < 1 ){
				viewHolder.btnAddAcc.setVisibility(View.INVISIBLE);
			}else{
				viewHolder.btnAddAcc.setVisibility(View.VISIBLE);
			}

			OnTouchListener touchListener = new OnTouchListener() {
				static final int DOUBLE_DURATION = 500;
				long lastTime = 0;

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN:
						long secondTime = System.currentTimeMillis();
						if (secondTime - lastTime < DOUBLE_DURATION) {
							/**
							 * 增加调料
							 */
							new AlertDialog.Builder(mContext)
									.setTitle("Select Seasoning")
									.setMultiChoiceItems(accName, accSelected,
											new DialogInterface.OnMultiChoiceClickListener() {

												@Override
												public void onClick(DialogInterface dialog, int which, boolean isChecked) {
													// TODO Auto-generated method stub
													accSelected[which] = isChecked;
												}
											}).setPositiveButton("OK", new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {
											// TODO Auto-generated method stub
											dishListItem.setAccSelected(accSelected);

											_viewHolder.tvAcc.setText(dishListItem.getAccNameSelected());
											_viewHolder.tvAccPrice.setText(dishListItem.getAccTotAmountString());
											updateTotalPrice();
										}
									}).setNegativeButton("Cancel", null).show();

						} else {
							//Log.d(TAG, "*************************************单击事件");
						}
						lastTime = secondTime;
						break;
					default:
						break;
					}
					return false;
				}

			};
			OnClickListener listener = new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (v == _viewHolder.btnCancel) {
						// delete item;
						//getItem(_position).setDishSelected(false);
						setDishUnselected(_position);
						remove(_position);
					} else if (v == _viewHolder.btnAddAcc) {

						/**
						 * 增加调料
						 */
						new AlertDialog.Builder(mContext)
								.setTitle("Select Seasoning")
								.setMultiChoiceItems(accName, accSelected,
										new DialogInterface.OnMultiChoiceClickListener() {

											@Override
											public void onClick(DialogInterface dialog, int which, boolean isChecked) {
												// TODO Auto-generated method stub
												accSelected[which] = isChecked;
											}
										}).setPositiveButton("OK", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										// TODO Auto-generated method stub
										dishListItem.setAccSelected(accSelected);

										_viewHolder.tvAcc.setText(dishListItem.getAccNameSelected());
										_viewHolder.tvAccPrice.setText(dishListItem.getAccTotAmountString());
										updateTotalPrice();
									}
								}).setNegativeButton("Cancel", null).show();
					}
				}

			};

			viewHolder.btnCancel.setOnClickListener(listener);
			viewHolder.btnAddAcc.setOnClickListener(listener);

			viewHolder.layoutListItem.setOnTouchListener(touchListener);

			int reqWidth = (int) mContext.getResources().getDimension(R.dimen.dish_thumbnail_width);
			//Log.d("DishListViewAdapter", "reqWidth: " + reqWidth);
			Bitmap bitmap = ImageLoader.decodeSampledBitmapFromResource(dishInfo.getImgFile(), reqWidth);
			viewHolder.imgDish.setImageBitmap(null);
			BitmapDrawable bd = new BitmapDrawable(bitmap);
			viewHolder.imgDish.setBackgroundDrawable((Drawable) bd);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return convertView;
	}

	protected void setDishUnselected(int position) {
		// TODO Auto-generated method stub
		try {
//			String name = getItem(position).getDish().getName();
//			int count = 0;
//			for (int i = 0; i < getCount(); i++) {
//				if (name.equals(getItem(i).getDish().getName())) {
//					count++;
//				}
//			}
//			
//			if (count <= 1) {
//				getItem(position).setDishSelected(false);
//			}
			getItem(position).setDishSelected(false);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public class ViewHolder {
		private RelativeLayout layoutListItem;
		private ImageView imgDish;
		private TextView tvDishName;
		/**
		 * not used
		 */
		private TextView tvDishDesc;
		private TextView tvDishPrice;
		private TextView tvAcc;
		private Button btnCancel;
		private Button btnAddAcc;
		private TextView tvAccPrice;

		public ViewHolder(View view) {
			this.layoutListItem = (RelativeLayout) view.findViewById(R.id.layout_listItem);
			this.imgDish = (ImageView) view.findViewById(R.id.imgDish);
			this.tvDishName = (TextView) view.findViewById(R.id.tvDishName);
			this.tvDishPrice = (TextView) view.findViewById(R.id.tvDishPrice);
			this.tvAcc = (TextView) view.findViewById(R.id.tvDishAcc);
			this.tvAccPrice = (TextView) view.findViewById(R.id.tvAccPrice);
			this.btnCancel = (Button) view.findViewById(R.id.btnCancel);
			this.btnAddAcc = (Button) view.findViewById(R.id.btnAddAcc);
		}

	}

	/* (non-Javadoc)
	 * @see android.widget.BaseAdapter#notifyDataSetChanged()
	 */
	@Override
	public void notifyDataSetChanged() {
		// TODO Auto-generated method stub
		super.notifyDataSetChanged();
		updateTotalPrice();
	}

	private void updateTotalPrice() {
		// TODO Auto-generated method stub
		float price;
		try {
			price = getAmount();
			billChanged.onOrderBillChangedListener(getCount(), price);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public float getAmount() {
		float price = 0;
		try {
			float dishTotal = 0;
			float accTotal = 0;
			for (int i = 0; i < getCount(); i++) {
				dishTotal = dishTotal + Float.parseFloat(getItem(i).getDish().getPrice());
				accTotal = accTotal + getItem(i).getAccTotAmount();
			}
			price = dishTotal + accTotal;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return price;
	}

	public void clearData() {
		// TODO Auto-generated method stub
		//listDishInfo.clear();
		try {
			for (int i = 0; i < getCount(); i++) {
				getItem(i).setDishSelected(false);
			}
			clear();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setOrderTime() {
		// TODO Auto-generated method stub
		SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		orderTime = sDateFormat.format(new java.util.Date());
	}

	public String getOrderTime() {
		if (orderTime == null || orderTime.length() < 1) {
			orderTime = "";
		}
		return orderTime;
	}

	public Object getOrderTime2() {
		// TODO Auto-generated method stub
		if (orderTime == null || orderTime.length() < 1) {
			orderTime = "";
		}
		String s = orderTime;
		s = s.replaceAll(" ", "");
		s = s.replaceAll("-", "");
		s = s.replaceAll(":", "");

		return s;
	}

}
