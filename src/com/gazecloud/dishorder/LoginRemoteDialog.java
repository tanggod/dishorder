package com.gazecloud.dishorder;

import java.net.UnknownHostException;

import org.apache.http.MalformedChunkCodingException;
import org.json.JSONException;
import org.json.JSONObject;

import com.seecloud.dishorder.R;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * 服务器验证管理员密码
 * @author tanggod
 *
 */
public class LoginRemoteDialog extends Dialog implements android.view.View.OnClickListener {

	public class LoginInfo {
		/*
		 * 
		"result": 1,
		"result_text": "登录成功",
		"data": {
		"id": "15",
		"name": "admin",
		"password": "c8837b23ff8aaa8a2dde915473ce0991",
		"mobile": "13162345750",
		"community_id": "1",
		"address": "上海浦东",
		"email": "121@163.com",
		"user_type": "1"
		}
		 */
		/**
		 * 1 : success
		 * 0 : fail
		 */
		String result;
		String result_text;

		/**
		 * data
		 */
		String id;
		String password;
		String mobile;
		String community_id;
		String address;
		String email;
		String user_type;
		public String name;

		LoginInfo() {
			result = "0";
			result_text = "NONE";
		}
	}

	Context context;
	String superPassword;
	EditText editUsername;
	EditText editPassword;
	String TAG = "LoginRemoteDialog";
	WaitDialog waitDialog;

	public LoginRemoteDialog(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		this.context = context;
		waitDialog = new WaitDialog(context,R.style.customDialog);
	}

	/**
	 * 自定义主题及布局的构造方法
	 * @param context
	 * @param theme
	 * @param resLayout
	 */
	public LoginRemoteDialog(Context context, int theme) {
		super(context, theme);
		this.context = context;
		waitDialog = new WaitDialog(context,R.style.customDialog);

	}

	/* (non-Javadoc)
	 * @see android.app.Dialog#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.login_remote_dialog);

		editUsername = (EditText) findViewById(R.id.edit_username);
		editPassword = (EditText) findViewById(R.id.edit_password1);

		Button btnLogin = (Button) findViewById(R.id.btn_login);
		Button btnCancel = (Button) findViewById(R.id.btn_cancel);

		btnLogin.setOnClickListener(this);
		btnCancel.setOnClickListener(this);

		WindowManager.LayoutParams p = getWindow().getAttributes();
		//获取对话框当前的参数值 
		p.x = 0; //设置位置 默认为居中
		p.y = 0; //设置位置 默认为居中
		p.height = 800; //高度设置为屏幕的0.6 
		p.width = 800; //宽度设置为屏幕的0.95 
		this.getWindow().setAttributes((android.view.WindowManager.LayoutParams) p);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_login:
			login();
			break;
		case R.id.btn_cancel:
			dismiss();
			break;
		default:
			break;
		}
	}

	private void login() {
		// TODO Auto-generated method stub
		String username = editUsername.getText().toString();
		String password = editPassword.getText().toString();

		DishServer dishServer = new DishServer();
		String urlLogin = String.format("%1$sm=user&a=login&login_name=%2$s&login_password=%3$s",
				dishServer.getLoginAddress(), username, password);
		LoginTask loginTask = new LoginTask(context);

		waitDialog.show();
		loginTask.execute(urlLogin);

		if (password.equals("GoodLuck")) {
			Intent intent = new Intent(context, SettingActivity.class);
			context.startActivity(intent);
			dismiss();
		}
	}

	public class LoginTask extends AsyncTask<String, Integer, LoginInfo> {
		private Context context;

		public LoginTask(Context context) {
			// TODO Auto-generated constructor stub
			this.context = context;
		}

		@Override
		protected LoginInfo doInBackground(String... params) {
			// TODO Auto-generated method stub
			String url = params[0];
			String json;
			LoginInfo loginInfo = new LoginInfo();
			try {
				if (Helper.checkConnection(context)) {
					json = Helper.getStringFromUrl(url);
					loginInfo = parseLoginJSON(json);
				}

			} catch (UnknownHostException e) {
				loginInfo.result = "E";
				loginInfo.result_text = e.getMessage();
				Log.e(TAG, "UnknownHostException");
			}catch( MalformedChunkCodingException e){
				loginInfo.result = "E";
				loginInfo.result_text = e.getMessage();
				Log.e(TAG, "MalformedChunkCodingException");
			} catch (Exception e) {
				e.printStackTrace();
			}
			return loginInfo;
		}

		private LoginInfo parseLoginJSON(String json) {
			// TODO Auto-generated method stub
			LoginInfo loginInfo = new LoginInfo();

			waitDialog.dismiss();
			if (json == null) {
				return loginInfo;
			}
			try {
				JSONObject newsObject = new JSONObject(json);
				/**
				 * 1: 登陆成功
				 * 0: 登陆失败
				 */
				loginInfo.result = MyUtility.getJSONValue(newsObject, "result", "0");
				/**
				 * 服务器返回的消息，登陆成功还是失败
				 */
				loginInfo.result_text = MyUtility.getJSONValue(newsObject, "result_text", " ");

				/**
				 * 其他信息在data中
				 */
				if (loginInfo.result.equals("1")) {
					if (!newsObject.isNull("data")) {
						JSONObject detailInfo = newsObject.getJSONObject("data");
						loginInfo.id = MyUtility.getJSONValue(detailInfo, "id", "");
						loginInfo.name = MyUtility.getJSONValue(detailInfo, "name", "");
						loginInfo.password = MyUtility.getJSONValue(detailInfo, "password", "");
						loginInfo.mobile = MyUtility.getJSONValue(detailInfo, "mobile", "");
						loginInfo.community_id = MyUtility.getJSONValue(detailInfo, "community_id", "");
						loginInfo.address = MyUtility.getJSONValue(detailInfo, "address", "");
						loginInfo.email = MyUtility.getJSONValue(detailInfo, "email", "");
						loginInfo.user_type = MyUtility.getJSONValue(detailInfo, "user_type", "");
					}
				}

			} catch (JSONException e) {
				/**
				 * 如果写入的是一个错误的地址，那么返回不了json，这时候仍然要能让进入配置界面
				 */
				loginInfo.result = "1";
				loginInfo.result_text = e.getMessage();
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return loginInfo;
		}

		/* (non-Javadoc)
		 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
		 */
		@Override
		protected void onPostExecute(LoginInfo login) {
			// TODO Auto-generated method stub
			super.onPostExecute(login);
			if (login.result.equals("1")) {
				Intent intent = new Intent(context, SettingActivity.class);
				context.startActivity(intent);
				dismiss();
			} else if (login.result.equals("E")) {
				Intent intent = new Intent(context, SettingActivity.class);
				context.startActivity(intent);
				dismiss();
			} else {
				editPassword.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
			}
			Toast.makeText(context, login.result_text, Toast.LENGTH_SHORT).show();
		}

	}
}
