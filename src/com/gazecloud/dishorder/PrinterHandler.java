package com.gazecloud.dishorder;

import java.lang.ref.WeakReference;

import com.zj.wfsdk.WifiCommunication;

import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

public class PrinterHandler extends Handler {
	private WeakReference<DishOrderMainActivity> mActivity;
	private WeakReference<WifiCommunication> wfComm;
	
	PrinterHandler(DishOrderMainActivity activity){
		mActivity = new WeakReference<DishOrderMainActivity>(activity);
	}

	
	public void setPrinter(WifiCommunication printer) {
		// TODO Auto-generated method stub
		wfComm = new WeakReference<WifiCommunication>(printer);
	}
	/* (non-Javadoc)
	 * @see android.os.Handler#handleMessage(android.os.Message)
	 */
	@Override
	public void handleMessage(Message msg) {
		// TODO Auto-generated method stub
		super.handleMessage(msg);
		DishOrderMainActivity activity = mActivity.get();
		WifiCommunication printer = wfComm.get(); 
		if( activity == null || printer == null ){
			return;
		}
		switch (msg.what) {
		case WifiCommunication.WFPRINTER_CONNECTED:
			activity.printMsg(printer);
			break;
		case WifiCommunication.WFPRINTER_DISCONNECTED:
			Toast.makeText(activity.getApplicationContext(), "Disconnect the WIFI-printer successful", Toast.LENGTH_SHORT)
					.show();
			activity.connFlag--;
			break;
		case WifiCommunication.SEND_FAILED:
			Toast.makeText(activity.getApplicationContext(), "Send Data Failed,please reconnect", Toast.LENGTH_SHORT).show();
			activity.connFlag--;
			break;
		case WifiCommunication.WFPRINTER_CONNECTEDERR:
			Toast.makeText(activity.getApplicationContext(), "Connect the WIFI-printer error", Toast.LENGTH_SHORT).show();
			activity.connFlag--;
			break;
		case WifiCommunication.CONNECTION_LOST:
			Toast.makeText(activity.getApplicationContext(), "Connection lost,please reconnect", Toast.LENGTH_SHORT).show();
			activity.connFlag--;
			break;
		default:
			break;
		}
	}


}
