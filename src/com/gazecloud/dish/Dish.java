package com.gazecloud.dish;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Dish implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 菜品分类
	 */
	protected String catalog;

	/**
	 * 菜品名称
	 */
	protected String name;
	
	/**
	 * 菜品名称，中文，打印用
	 */
	protected String name_cn;

	/**
	 * 菜品介绍，描述
	 */
	protected String desc;
	/**
	 * 菜品图片链接
	 */
	protected String imgUrl;

	/**
	 * 菜品图片文件所在位置
	 */
	protected String imgFile;

	/**
	 * 菜品价格
	 */
	protected float price;

	/**
	 * 辅料列表
	 */
	List<DishAccessory> dishAcc;
	
	/**
	 * 如果是套餐，则subDish指套餐下面有哪些菜，否则为null
	 */
	List<Dish>	subDish;

	public String getImgUrl() {
		return imgUrl;
	}

	public void setName(String s) {
		// TODO Auto-generated method stub
		name = s;
	}

	public void setDesc(String s) {
		// TODO Auto-generated method stub
		desc = s;
	}

	public void setImgUrl(String s) {
		// TODO Auto-generated method stub
		imgUrl = s;
	}

	public void setImgFile(String imagePath) {
		// TODO Auto-generated method stub
		imgFile = imagePath;
	}

	public void setPrice(float f) {
		// TODO Auto-generated method stub
		price = f;
	}

	public void setPrice(String s) {
		// TODO Auto-generated method stub
		try {
			price = Float.parseFloat(s);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 添加辅料信息
	 * @param dishAccessory
	 */
	public void addAccItem(DishAccessory acc) {
		// TODO Auto-generated method stub
		if( dishAcc == null){
			dishAcc = new ArrayList<DishAccessory>();
		}
		dishAcc.add(acc);
	}

	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}

	/**
	 * 返回价格
	 * @return String
	 */
	public String getPrice() {
		// TODO Auto-generated method stub
		String s = String.format(Locale.getDefault(),"%.2f", price);
		return s;
	}

	/**
	 * 获取辅料列表
	 * @return List<DishAccessory>
	 */
	public List<DishAccessory> getAcc() {
		// TODO Auto-generated method stub
		return dishAcc;
	}
	public int getAccSize() {
		// TODO Auto-generated method stub
		if( dishAcc == null)
			return 0;
		else
			return dishAcc.size();
	}

	/**
	 * 返回菜品图片所在路径
	 * @return String
	 */
	public String getImgFile() {
		// TODO Auto-generated method stub
		return imgFile;
	}

	public String getDesc() {
		// TODO Auto-generated method stub
		return desc;
	}
	public String getCatalog(){
		return catalog;
	}
	
	public void setCatalog(String s) {
		// TODO Auto-generated method stub
		catalog = s;
	}
	public List<Dish> getSubDish(){
		return subDish;
	}

	public void addSubDish(Dish common) {
		// TODO Auto-generated method stub
		if( subDish == null){
			subDish = new ArrayList<Dish>();
		}
		subDish.add(common);
	}

	public void setCNName(String s) {
		// TODO Auto-generated method stub
		name_cn = s;
	}

	public String getCNName() {
		// TODO Auto-generated method stub
		return name_cn;
	}
	public String getENName() {
		// TODO Auto-generated method stub
		return name;
	}
	public String getAllName() {
		// TODO Auto-generated method stub
		return name + "," + name_cn;
	}

}
