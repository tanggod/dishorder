package com.gazecloud.dish;

import java.io.Serializable;

public class DishAccessory implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private String img;
	private float price;

	public void setName(String s) {
		name = s;
	}

	public void setPrice(String s) {
		// TODO Auto-generated method stub
		try {
			if (s == null) {
				price = 0;
				return;
			} else if (s != null & s.length() == 0) {
				price = 0;
				return;
			}
			price = Float.parseFloat(s);
		} catch (Exception e) {
			e.printStackTrace();
			price = 0;
		}
	}

	public String getName() {
		return name;
	}

	public float getPrice() {
		// TODO Auto-generated method stub
		return price;
	}
}
