package com.gazecloud.dish;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;


public class DishSet implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String TAG = "DishSet";
	/**
	 * 普通菜品，套餐
	 */
	private Map<String,List<Dish>> mDishMap;
	
	
	public DishSet(){
		mDishMap = new HashMap<String,List<Dish>>();
	}
	
	/**
	 * 这里算法不对，还需要纠正
	 * @return
	 */
	public int getSize() {
		// TODO Auto-generated method stub
		int size = 0;
		for( String key : mDishMap.keySet() ){
			size = size + mDishMap.get(key).size();
		}
		return size;
	}
	/**
	 * 返回菜品列表
	 * @return  Map<String,List<CommonDish>>
	 */
	public Map<String,List<Dish>> getDish() {
		// TODO Auto-generated method stub
		return mDishMap;
	}
	/**
	 * 按照Key值排序
	 * @return
	 */
	public List<Entry<String,List<Dish>>> getSortedMapKey(){
		List<Entry<String,List<Dish>>> list = new ArrayList<Entry<String,List<Dish>>>(mDishMap.entrySet());
		Collections.sort(list, new Comparator<Map.Entry<String, List<Dish>>>() {
			@Override
			public int compare(Entry<String, List<Dish>> lhs, Entry<String, List<Dish>> rhs) {
				// TODO Auto-generated method stub
				return lhs.getKey().compareTo(rhs.getKey());
			}
		});
		return list;
	}
	public void addDish(Dish dish) {
		// TODO Auto-generated method stub
		String catalog = dish.getCatalog();
		
		if( catalog == null){
			catalog = "undefined";
		}else if( catalog.length() == 0 ){
			catalog = "undefined";
		}
		
		List<Dish> list = mDishMap.get(catalog);
		if( list == null){
			list = new ArrayList<Dish>();
		}
		list.add(dish);
		mDishMap.put(catalog,list);
	}
}
